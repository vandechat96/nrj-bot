const {roleMenu,asyncForEach} = require("../../utils/utils");
const GuildConfig = require('../../database/schemas/GuildConfig')
const GuildRoleMenu = require('../../database/schemas/GuildRoleMenu')
const ActiveServers = require('../../database/schemas/ActiveServers')
const Scores = require('../../database/schemas/Scores')
//const HallOfFamePlayerResults = require('../../database/shemas/HallOfFamePlayerResults')
const TournoiSave = require('../../database/schemas/TournoiSave')

const {TournoiManager} = require("../../models/TournoiManager");


module.exports = async (client) => {
    console.log(`${client.user.tag} has logged in.\n`);

    saveLoop(client)
    

    let fct = async (guildId) => {
        guildId=guildId[0]
        client.servConfig.set(guildId, await GuildConfig.findOne({guildId: guildId}) || await GuildConfig.create({guildId: guildId}))
        let active =  await ActiveServers.findOne({guildId: guildId}) || await ActiveServers.create({guildId: guildId})
        client.activeServers.set(guildId,active.active)

        let menus = await GuildRoleMenu.find({guildId: guildId})
        if (!menus || !menus[0]) return;
        client.roleMenus.set(guildId, menus)
        for (let i = 0; i < menus.length; i++) {
            roleMenu(client, menus[i]).catch(console.error)
        }
    }

    console.log('Start Guild config initialization')
    await asyncForEach(client.guilds.cache,fct)
    console.log('Guild config initialized')

    /*
    if (client.user.id === '457838031567716353'){
        console.log('Start tournament initialization')
        for await (const tournoi of TournoiSave.find()) {
            let guild = await client.guilds.fetch(tournoi.guildId)
            if(!guild) continue
            let channel = guild.channels.cache.get(tournoi.channelId)
            let author = await client.users.fetch(tournoi.authorId)
            if (!author || !channel) continue
            let config = client.servConfig.get(guild.id)

            try{
                let manager = new TournoiManager(client,tournoi.type,tournoi.args,{author,channel,guild,config})
                let msg = await channel.messages.fetch(tournoi.msgId)
                let msgEmbed = await channel.messages.fetch(tournoi.msgEmbedId)
                manager.restart(tournoi,msgEmbed,msg)
            } catch (e){
                console.error('Manager could not initialized tournament deleted')
                TournoiSave.findByIdAndDelete(tournoi._id).exec()
            }
        }
        console.log('Tournament initialized')
    }*/

    client.ready = true
    client.user.setPresence({
        activities: [{name:'N!help'}],
    });
}

function saveLoop(client){

    client.scoresChange.forEach((val)=>{
        Scores.findOneAndUpdate({guildId:val.guildId,userId:val.userId},{points:val.points,level:val.level}).exec()
    })
    client.scoresChange.clear()
    setTimeout(()=>saveLoop(client),10000)
}
