const GuildConfig = require('../../database/schemas/GuildConfig')
const ActiveServers = require('../../database/schemas/ActiveServers')

module.exports = async (client, guild) => {
    try {
        const guildConfig = await GuildConfig.create({
            guildId: guild.id
        })
        client.servConfig.set(guild.id, guildConfig)

        client.activeServers.set(guild.id, await ActiveServers.create({guildId: guild.id}))
        client.roleMenus.set(guild.id, [])
  } catch (err) {
        console.error(err)
    }
};
