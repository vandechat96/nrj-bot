const GuildConfig = require('../../database/schemas/GuildConfig')
const {isAdmin} = require("../../utils/utils");
let cooldown = new Set();
const banword = require(`../../utils/banword`);
const {DMChannel} = require("discord.js");
module.exports = async (client, message) => {
    if(!client.ready) return
    if (message.author.bot) return;
    let config
    let prefix
    let guild
    if (message.channel.type === DMChannel) {
        prefix = ''
    } else {
        guild = message.guild;
	if (!guild) return;
        let active = client.activeServers.get(guild.id)
        if(!active) return
        config = client.servConfig.get(guild.id)

        if (!config) {
            config = await GuildConfig.findOne({guildId: guild.id})
            if (!config){
                try {
                    config = await GuildConfig.create({
                        guildId: guild.id
                    })
                    client.servConfig.set(guild.id,config)
                } catch (err) {
                    return console.error(err)
                }
            }
        }
        if (message.channel.id === config.get('commChannel')) {
            const watch = require(`../../utils/watch`);
            watch.run(client, message).catch(console.error)
        }
        prefix = config.prefix

        if (message.channel.id === config.commChannel) {
            const watch = require(`../../utils/watch`);
            watch.run(client, message).catch(console.error)
        }
        if (client.user.id === "719987677122003036"){
            banword.run(client, message,config).catch(console.error)
        }

    }

    const startsPrefix = message.content.startsWith(prefix)

    if (!startsPrefix && !message.mentions.users.has(client.user.id)) return require(client.commands.get('level')).run(client, message, message.content.trim().split(/\s+/), {config});

    let preargs = startsPrefix?message.content.slice(  prefix.length):message.content.replace(client.user.toString(),'')
    let args = preargs.trim().split(/\s+/);
    let cmdName = args.shift().toLowerCase();
    let cmdPath = client.commands.get(cmdName)
    if (!cmdPath) return
    let cmd = require(cmdPath)
    cmdName = cmd.info.name

    if (message.channel.type === 'dm' && !cmd.info.dm) return;

    delete require.cache[require.resolve('../../json/modules.json')];
    const modules = require('../../json/modules.json')
    const index = modules.findIndex(cmd => cmd.file === cmdName)
    if (index !== -1 && modules[index].active) {
        try {
            if (cooldown.has(message.author.id)){
                message.delete().catch(console.error);
                return message.reply(`Vous devez attendre ${config.cooldown} secondes entre chaque commandes !`).catch(console.error)
            }
            if(guild && !isAdmin(message,config)) cooldown.add(message.author.id);

            delete require.cache[require.resolve(cmdPath)];
            console.log('run ' + cmdName)
            console.log('msg : ' + message.content)
            console.log('args : ' + args)
            if (guild) console.log(`guild : ${guild.name} (${guild.id})`)
            console.log(`user : ${message.author.tag} (${message.author.id})`)

            cmd.run(client, message, args, {config})

            if(message.channel.type !== 'dm'){
                setTimeout(() => {
                    cooldown.delete(message.author.id)
                }, config.cooldown * 1000)
            }
        } catch (e) {
            console.log(e.stack)
        }

    }
};

