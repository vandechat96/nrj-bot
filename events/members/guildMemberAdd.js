module.exports = async (client,member) => {
    const guild = member.guild;
    let config = client.servConfig.get(guild.id);
    let channel = guild.channels.cache.get(config.welcomeChannel);
    if (channel) {
        if (guild.id === '767138908747464734') {
            const Discord = require('discord.js')
            let embed = new Discord.MessageEmbed()
                .setColor(client.color)
                .setFooter("© "+client.user.username)
                .setTimestamp()
                .setThumbnail(member.user.displayAvatarURL())
                .addField(`HeyCoucou! ${member.user.username} !`,
                    `Welcome chez les Naknaks. Merci d’aller lire et accepter le règlement juste en dessous de ce salon. Ainsi tu aura accès au reste de notre serveur. :sparkles:`)
            channel.send({embeds : [embed]});
            return channel.send(`<@${member.user.id}>` )
        }
    }
}