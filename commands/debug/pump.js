const {sendDm,getFrDate,getEnDate} = require("../../utils/utils")
module.exports = {
    run: async (client, message, args, opt) => {
        message.delete()

        const Discord = require('discord.js');
        const user = message.mentions.users.first() || await client.users.fetch(args[0]).catch(console.error) || message.author;

        if (!user) return message.channel.send('Mauvaise mention !').catch(console.error);

        const {config} = opt
        const language = config.language
        const localization = client.localization.get(language)
        const dateFct = language === "french" ? getFrDate : getEnDate
        const serverInfo = localization.server_info

        let readySince = new Date(Date.now() - user.client.readyAt)
        let createdSince = new Date(Date.now() - user.createdAt)

        const embed = new Discord.MessageEmbed()
            .setColor(client.color)
            .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
            //.setImage(user.displayAvatarURL())
            .addField("Tag", `\`\`\`${user.tag}\`\`\``, true)
            .addField("ID", `\`\`\`${user.id}\`\`\``, true)

            .addField("Bot", `\`\`\`${user.bot}\`\`\``, true)
            .addField("Avatar URL", `[URL](${user.displayAvatarURL()})`, true)
            .addField("Message", `\`\`\`${user.lastMessage}\`\`\``, true)
            .addField("Ready at", `\`\`\`${dateFct(user.client.readyAt)} (${serverInfo.since.replace("{day}", readySince.getDate()).replace('{month}', readySince.getMonth()).replace('{year}', readySince.getFullYear() - 1970)})\`\`\``)
            .addField("Creation", `\`\`\`${dateFct(user.createdAt)} (${serverInfo.since.replace("{day}", createdSince.getDate()).replace('{month}', createdSince.getMonth()).replace('{year}', createdSince.getFullYear() - 1970)})\`\`\``, )

        if (user.presence.clientStatus){
            let device = user.presence.user.presence.clientStatus.hasOwnProperty('desktop')?'desktop' :user.presence.clientStatus.hasOwnProperty('mobile')?'mobile': user.presence.clientStatus.hasOwnProperty('web')?'web':'unknown'

            let activities = ''
            for (const activity of  user.presence.activities) {
                activities+=`${activity.name} (${activity.type}) :
            ${activity.details||''} ${activity.state}\n`

            }
            embed
                .addField("Presence", `\`\`\`Status : ${user.presence.status}\nDevice : ${device}\n${activities}\`\`\``)
        }


        if (message.channel.type !== 'dm') {
            let guild = message.guild
            let member = guild.members.cache.get(user.id)
            if (member) {
                embed.addField('Roles:', member.roles.cache.map(r => `${r.name}`).join(' | '), true)
            }
        }

        await sendDm(user, embed)
    },
    aliases: [],
    description: {french: "", english: ''},
    help: [{cmd: "", desc: ""}],
    info: {dm: false, name: 'pump', category: "hidden"}
}
