const ActiveServers = require('../../database/schemas/ActiveServers')
module.exports = {
    run: async (client, message, args) => {
        const Discord = require('discord.js')
        const config = require("../../json/config.json");
        let id = message.author.id;
        if (id !== config.ownerId && id !== '290489079617814529') return

        if (client.guilds.cache.has(args[0])) {
            let guild = client.guilds.cache.get(args[0])

            if (args[1] === 'leave') {
                guild.leave().catch(console.error)
                let txt = `Guilde quité => ${guild.name} : ** \`${guild.id}\`**\n `
                return message.channel.send(txt)
            } else if (args[1] === 'invite') {
                let invite = await guild.fetchInvites()
                if (invite.size === 0) {
                    invite = await guild.channels.cache.filter(channel => channel.permissionsFor(guild.me).has('CREATE_INSTANT_INVITE')).first().createInvite({
                        maxAge: 30,
                        maxUses: 1
                    }).catch()
                } else {
                    invite = invite.first()
                }

                let txt = `Invitation pour => ${guild.name} : ** \`${guild.id}\`**\n${invite}`
                return message.channel.send(txt)
            } else {
                let active = !client.activeServers.get(guild.id)

                client.activeServers.set(guild.id, active)
                ActiveServers.findOneAndUpdate({guildId: guild.id}, {active: active}).exec()

                let txt = `${guild.name} : ** \`${guild.id}\`** Actvive : ${active}\n `
                return message.channel.send(txt)
            }


        }

        let embed = newEmbed();

        let i = 0
        for (let guild of client.guilds.cache) {
            let active = client.activeServers.get(guild[0])
            let txt = `${guild[1].name} : ** \`${guild[0]}\`** \n `
            if (active) {
                embed.addField(txt, "active: ✅", true);
            } else {
                embed.addField(txt, "active: ❌", true);
            }
            i++
            if (i !== 0 && i % 24 === 0) {
                message.channel.send({embeds : [embed]}).catch(console.error);
                embed = newEmbed()
            }
        }

        message.channel.send({embeds : [embed]}).catch(console.error);

        function newEmbed() {
            return new Discord.MessageEmbed()
                .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                .setColor(client.color)
                .setTitle("__**Liste des serveurs**__")
        }
    },
    aliases: [],
    description: {french: "", english: ''},
    help: [],
    info: {dm: false, name: 'karma', category: "hidden"}
}
