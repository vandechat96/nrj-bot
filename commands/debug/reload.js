const {registerLocalizations, registerEvents,registerCommands,registerClean} = require('../../utils/registry');
module.exports = {
    run: async (client, message, args) => {
        const config = require("../../json/config.json");

        if (message.author.id !== config.ownerId) return

        let cmd = client.commands.get(args[0])
        if (cmd) {
            try {
                delete require.cache[require.resolve(cmd)]
            } catch (e) {
                console.error(e)
                return message.channel.send('Impossible de rechargé la commande ' + args[0] + ' !').catch(console.error)
            }
            message.channel.send('Commande ' + args[0] + ' rechargé avec succès !').catch(console.error)
        } else if (args[0] === 'localization') {
            registerLocalizations(client, '../strings').catch(console.error)
            message.channel.send('Localization rechargé avec succès !').catch(console.error)
        }else if(args[0] === 'events'){
			registerEvents(client,'../events').catch(console.error)
			message.channel.send('Events rechargé avec succès !').catch(console.error)
		} else if(args[0] === 'commands'){
            registerCommands(client,'../commands').catch(console.error)
            message.channel.send('Commands rechargé avec succès !').catch(console.error)
        } else if(args[0] === 'everything'){
            registerClean(client,'../json').catch(console.error)
            registerClean(client,'../utils').catch(console.error)
            registerClean(client,'../models').catch(console.error)

            registerCommands(client,'../commands').catch(console.error)
            registerEvents(client,'../events').catch(console.error)
            registerLocalizations(client, '../strings').catch(console.error)
            message.channel.send('Tout à été rechargé avec succès !').catch(console.error)
        }
        else {
            try {
                delete require.cache[require.resolve('../../' + args[0])]
            } catch (e) {
                return message.channel.send('Impossible de rechargé le fichier ' + args[0] + ' !').catch(console.error)
            }
            message.channel.send('Fichier ' + args[0] + ' rechargé avec succès !').catch(console.error)
        }

    },
    aliases: ['rl'],
    description: {french: 'Reload a command'},
    help: [],
    info: {dm: false, name: "reload", category: "hidden"}
}
