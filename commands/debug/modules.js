module.exports = {
    run: async (client, message, args) => {
        let i;
        const Discord = require('discord.js');
        const fs = require("fs");
        let modules = JSON.parse(fs.readFileSync("./json/modules.json", "utf8"));
        const config = require("../../json/config.json");

        if (message.author.id === config.ownerId) {
            if (args[0] === "set") {
                if (args[1] && args[1] !== "Modules") {
                    for (i = 0; i < modules.length; i++) {
                        if (modules[i].file === args[1]) {
                            modules[i].active = !modules[i].active
                        }
                    }
                }
            } else if (args[0] === "add") {
                if (args[1]) {
                    let id = modules.length;
                    modules[id] = {
                        file: args[1],
                        active: true,
                    };
                }
            }
            fs.writeFile("./json/modules.json", JSON.stringify(modules), (err) => {
                if (err) console.error(err)
            })

            let embed = newEmbed()

            function newEmbed() {
                return new Discord.MessageEmbed()
                    .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                    .setColor(client.color)
                    .setTitle("__**Modules**__")
            }

            for (i = 0; i < modules.length; i++) {
                if (modules[i].active && client.commands.has(modules[i].file)) {
                    embed.addField(modules[i].file, "active: ✅", true);
                } else {
                    embed.addField(modules[i].file, "active: ❌", true);
                }
                if (i !== 0 && i % 24 === 0) {
                    message.channel.send({embeds : [embed]}).catch(console.error);
                    embed = newEmbed()
                }
            }
            message.channel.send({embeds : [embed]}).catch(console.error);
        }
    },
    aliases: ['mds'],
    description: {french: 'modules'},
    help: [],
    info: {dm: false, name: "modules", category: "hidden"}
}

