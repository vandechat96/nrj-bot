const {isAdmin} = require("../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        let config = opt.config
        const localization = client.localization.get(config.language)

        if (!isAdmin(message, config)) return message.reply(localization.general.no_perm || "no_perm");

        let guild = message.guild
        let user = message.mentions.members.first() || guild.members.cache.get(args[0])

        if (user) {
            if (isNaN(args[1])) return message.channel.send(localization.general.nan || "nan")
            let limit = args[1]
            if (limit >= 100) return message.channel.send(localization.general.big || "big")
            try {
                let fetchedMessages = await message.channel.messages.fetch({limit});
                let filteredMessages = fetchedMessages.filter(msg => msg.author.id === user.id);
                let deletedMessages = await message.channel.bulkDelete(filteredMessages);
                message.channel.send(`${deletedMessages.size} ${localization.general.msg_deleted || "msg_deleted"}`);
            } catch (err) {
                console.err(err);
            }
        } else if (!isNaN(args[0]) && args[0] >= 1) {
            let nbr = parseInt(args[0]) + 1;
            let channel = message.channel
            if (nbr < 50) {
                channel.bulkDelete(nbr).catch(() => {
                    message.reply(localization.general.not_deleted || "not_deleted").catch(console.error)
                }).then(() => message.reply(`${args[0]} ${localization.general.msg_deleted || "msg_deleted"}`).then((msg) => {
                    setTimeout(() => {
                        if (!msg.deleted) msg.delete().catch(console.error)
                    }, 5000)
                })).catch(console.error)
            } else {
                while (nbr > 50) {
                    nbr = nbr - 50;
                    await channel.bulkDelete(50).catch(console.error)
                }
                await channel.bulkDelete(nbr).then(() => message.reply(`${args[0]} ${localization.general.msg_deleted || "msg_deleted"}`).then((msg) => {
                    setTimeout(() => {
                        if (!msg.deleted) msg.delete().catch(console.error)
                    }, 5000)
                })).catch(console.error)
            }
        } else {
            return message.channel.send(localization.general.nan || "nan");
        }

    },
    aliases: ['purge', 'prune', 'clean'],
    description: {
        french: 'Supprime un certain nombre de message d\'un utilisateur ou pas dans un salon.',
        english: 'Deletes a number of messages from a user or not in a channel.'
    },
    help: [{cmd: "clear {nbr}", desc: "clear_desc_0"}, {cmd: "clear {user} {nbr}", desc: "clear_desc_1"}],
    info: {dm: false, name: 'clear', category: "mod"}
}