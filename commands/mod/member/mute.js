const {sendDm} = require("../../../utils/utils");
const {isAdmin} = require("../../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        let ok;
        const {config} = opt
        const localization = client.localization.get(config.language)
        const role = message.guild.roles.cache.get(config.muteRole)
        if (!role) return message.channel.send(localization.general.no_role || "no_role").catch(console.error);
        if (!isAdmin(message, config) && !message.member.hasPermission(['KICK_MEMBERS', 'BAN_MEMBERS']))
            message.channel.send(localization.general.no_perm || "no_perm");
        else {
            const member = message.mentions.members.first() || message.guild.members.cache.get(args[0]);

            if (member) {
                ok = true;
                if (member.roles.cache.has(role.id)) {
                    member.roles.remove(role).catch(error);
                    let baseRole = message.guild.roles.cache.get(config.baseRole);
                    if (baseRole) await member.roles.add(baseRole).catch(error);
                    if (ok) {
                        await sendDm(member.user, localization.unmute.unmuted0 || "unmuted");
                        message.channel.send(`${member} ${localization.unmute.unmuted1 || "unmuted"}`).catch(console.error);
                    }
                } else {
                    if (member.roles.cache.size > 1) member.roles.cache.forEach((key, value) => {
                        if (key.name === "@everyone") return
                        member.roles.remove(message.guild.roles.cache.get(value), "Muted").catch(error);
                    })
                    await member.roles.add(role, "Mute").catch(error);
                    if (ok) {
                        await sendDm(member.user, localization.mute.muted0 || "muted")    ;
                        message.channel.send(`${member} ${localization.mute.muted1 || "muted"}`).catch(console.error);
                    }

                }

            } else
                message.channel.send(localization.general.wrong_mention || "wrong_mention")
        }

        function error(e) {
                console.error(e)
            message.channel.send(localization.general.bot_no_perm || "bot_no_perm")
            ok = false
        }
    },
    aliases: [],
    description: {french: 'Mute un membre.', english: 'Mutes a user.'},
    help: [{cmd: "mute {user} ", desc: "mute_desc_0"}],
    info: {dm: false, name: 'mute', category: "mod"}
}