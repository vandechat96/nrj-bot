const mute = require('./mute.js')
module.exports = {
    run: async (client, message, args, opt) => {
        mute.run(client, message, args, opt).catch(console.error)
    },
    aliases: [],
    description: {french: 'Demute un membre.', english: 'Unmutes a user.'},
    help: [{cmd: "unmute {user} ", desc: "unmute_desc_0"}],
    info: {dm: false, name: 'unmute', category: "mod"}
}