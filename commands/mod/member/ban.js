const {isAdmin, separateBrack} = require("../../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        if (!isAdmin(message, config) && !message.member.hasPermission("BAN_MEMBERS")) {
            message.channel.send(localization.general.no_perm || "no_perm");
        } else {
            const member = message.mentions.members.first() || await message.guild.members.fetch(args[0]);
            if (!member) return message.channel.send(localization.general.wrong_mention || "wrong_mention").catch(console.error)
            let reason = separateBrack(args, 2)
            let days = args[1] || 0

            try {
                if (member.bannable) {
                    member.ban({
                        days: days,
                        reason: reason
                    }).then(() => message.channel.send(`**${member.displayName}** ${localization.ban.ok || "ban_ok"}`))
                }
            } catch (err) {
                message.channel.send(localization.ban.ko || "ban_ko").catch(console.error)
            }

        }
    },
    aliases: [],
    description: {french: "Ban un membre du serveur.", english: 'Bans a guild member.'},
    help: [{cmd: "ban {user} {days} {reason}", desc: "ban_desc_0"}],
    info: {dm: false, name: 'ban', category: "mod"}
}