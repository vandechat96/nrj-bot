const {isAdmin, separateBrack} = require("../../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        if (!isAdmin(message, config) && !message.member.hasPermission("BAN_MEMBERS")) {
            message.channel.send(localization.general.no_perm || "no_perm");
        } else {
            let user
            try {
                user = await client.users.fetch(args[0]);
            } catch (e){
                user = undefined
            }
            if (!user) return message.channel.send(localization.general.wrong_mention || "wrong_mention").catch(console.error)
            let reason = separateBrack(args, 1)

            try {
                message.guild.unban(user, reason).then(() => message.channel.send(`**${user.username}** ${localization.unban.ok || "unban_ok"}`))
            } catch (err) {
                message.channel.send(localization.unban.ko || "unban_ko").catch(console.error)
            }

        }
    },
    aliases: [],
    description: {french: "Deban un utilisateur.", english: 'Unban a user.'},
    help: [{cmd: "unban {user_id}", desc: "unban_desc_0"}],
    info: {dm: false, name: 'unban', category: "mod"}
}