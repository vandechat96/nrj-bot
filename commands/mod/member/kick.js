const {isAdmin} = require("../../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        if (!isAdmin(message, config) && !message.member.hasPermission('KICK_MEMBERS'))
            message.channel.send(localization.general.no_perm || "no_perm");
        else {
            const member = message.mentions.members.first() || message.guild.members.cache.get(args[0]);
            if (member) {
                try {
                    await member.kick()
                    message.channel.send(`**${member.displayName}** ${localization.kick.ok || "kick_ok"}`).catch(console.error)
                } catch (err) {
                    console.error(err)
                    message.channel.send(localization.kick.ko || "kick_ko").catch(console.error)
                }
            } else {
                message.channel.send(localization.general.wrong_mention || "wrong_mention")
            }
        }
    },
    aliases: [],
    description: {french: "Kick un utilisateur.", english: 'Kicks a user'},
    help: [{cmd: "kick {user} {reason}", desc: "kick_desc_0"}],
    info: {dm: false, name: 'kick', category: "mod"}
}