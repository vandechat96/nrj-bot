const {isAdmin} = require("../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization=client.localization.get(config.language)

        if (!isAdmin(message, config)) return message.reply(localization.general.no_perm).catch(console.error);

        const muteRole = message.guild.roles.cache.get(config.muteRole)
        if (!muteRole) return message.channel.send(localization.general.no_role || "no_role").catch(console.error);

        const baseRole = message.guild.roles.cache.get(config.baseRole)
        if (!baseRole) return message.channel.send(localization.general.no_role || "no_role").catch(console.error);


        let members = [];
        let tour = 0;

        for (let msg of message.channel.messages.cache) {
            let member = msg[1].member;
            let index = members.indexOf(member);
            if (index === -1) members.push(member);
            tour++;
            if (members.length === 15 || tour >= 1000) break;
        }


        for (let member of members) {
            if (member.roles.has(muteRole.id)) {
                await member.roles.remove(muteRole).catch(console.error).catch(console.error);
                await member.roles.add(baseRole).catch(console.error.catch(console.error));

            } else if (member.roles.cache.has(baseRole.id) && !member.roles.cache.has(muteRole) && !isAdmin(message, config)) {
                await member.roles.cache.forEach((key, value) => {
                    member.roles.remove(message.guild.roles.get(value)).catch(console.error);
                });
                await member.roles.add(muteRole).catch(console.error).catch(console.error);
            }
        }
    },
    aliases: ["stfu"],
    description: {french: "Mute les 15 derniers membres.", english: 'Mute the last 15 members.'},
    help: [],
    info: {dm: false, name: 'muteall', category: "mod"}
}
