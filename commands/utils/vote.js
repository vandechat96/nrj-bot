module.exports = { 
    run: async(client, message, args,opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        args = args.join(' ').split(',')
        let letters=[{emoji:'🇦',nbr:0},{emoji:'🇧',nbr:0},{emoji:'🇨',nbr:0},{emoji:'🇩',nbr:0},{emoji:'🇪',nbr:0},{emoji:'🇫',nbr:0},{emoji:'🇬',nbr:0},{emoji:'🇭',nbr:0},{emoji:'🇮',nbr:0},{emoji:'🇯',nbr:0},{emoji:'🇰',nbr:0},{emoji:'🇱',nbr:0},{emoji:'🇲',nbr:0},{emoji:'🇳',nbr:0},{emoji:'🇴',nbr:0},{emoji:'🇵',nbr:0},{emoji:'🇶',nbr:0},{emoji:'🇷',nbr:0},{emoji:'🇸',nbr:0},{emoji:'🇹',nbr:0},{emoji:'🇺',nbr:0},{emoji:'🇻',nbr:0},{emoji:'🇼',nbr:0},{emoji:'🇽',nbr:0},{emoji:'🇾',nbr:0},{emoji:'🇿',nbr:0}];
        if (!args[4]) return message.channel.send(localization.general.not_complete)

        let titre= args[1]+'\n';

        let qTotal = 0;
        for (let i = 2; i < 8; i++) {
            if (args[i]) {
                qTotal++
            }
        }
        let msgVote= "";
        for (let i = 0; i < qTotal; i++) {
            let nbr= i+2;
            msgVote+= letters[i].emoji+' : '+args[nbr]+'\n'
        }
        message.delete().catch(console.error);

        const Discord = require('discord.js');
        const embed = new Discord.MessageEmbed()
            .setTitle(localization.vote.title)
            .setColor(client.color)
            .setTimestamp()
            .addField(titre,msgVote);

        message.channel.send({embeds : [embed]}).then(async (msg)=> {
            for (let i = 0; i < qTotal; i++) {
                await msg.react(letters[i].emoji).catch(console.error)
            }
            const filter = (r, user) => !user.bot;

            const time = parseInt(args[0]);
            if (isNaN(time)) return message.channel.send(localization.general.nan)
            const collector = msg.createReactionCollector({filter, time: time * 1000,dispose:true})


            let userChoice = new Map()

            collector.on('collect', (r, user) => {
                if (userChoice.has(user.id)) {
                    let choice = userChoice.get(user.id)
                    let index = letters.findIndex((obj) => obj.emoji === choice)
                    letters[index].nbr--;
                }

                let index = letters.findIndex((obj) => obj.emoji === r.emoji.name)
                letters[index].nbr++;
                userChoice.set(user.id, r.emoji.name)
            })

            collector.on('remove', (r, user) => {

                if (userChoice.has(user.id)) {
                    let choice = userChoice.get(user.id)
                    if (r.emoji.name !== choice) return
                    let index = letters.findIndex((obj) => obj.emoji === choice)
                    letters[index].nbr--;
                    userChoice.delete(user.id)
                }
            })

            collector.on('end', () =>{
                let msgFinal = ``;
                for (let i = 0; i < qTotal; i++) {
                    let nbr= i+2;
                    msgFinal+= `${localization.vote.count} ${args[nbr]} : ${ letters[i].nbr}\n`
                }

                message.channel.send(new Discord.MessageEmbed()
                    .setTitle(localization.vote.title)
                    .setColor(client.color)
                    .setTimestamp()
                    .addField(localization.vote.question+' : ',titre)
                    .addField(localization.vote.result+' :',msgFinal))

            })

        })


    },
   aliases: ['poll','sondage'],
   description: {french:"Crée un sondage.",english:  'Create a poll.'},
   help:[{cmd:"vote {time (seconds),{title},{choice 1},{choice 2},{...},...",desc:"vote_desc_0"}],
   info:{dm:false,name:'vote',category:"utils"}
}
