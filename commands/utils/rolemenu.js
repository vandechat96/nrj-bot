const {isAdmin, sendDm, firstUpper, roleMenu} = require("../../utils/utils");
const GuildRoleMenu = require('../../database/schemas/GuildRoleMenu')
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)
        if (!isAdmin(message, config) && !message.member.hasPermission('MANAGE_ROLES')) return message.channel.send(localization.general.no_perm || "no_perm");
        const Discord = require('discord.js')

        if (args[0] === 'modify') {
            message.delete()
            let menus = client.roleMenus.get(message.guild.id);
            if (args[1]) {
                let id = args[1];
                if (!menus[id]) return message.channel.send(localization.rolemenu.not_menu).catch(console.error)
                let rolesMap = menus[id].rolesMap;

                let guild = client.guilds.cache.get(menus[id].guildId);
                let channel = guild.channels.cache.get(menus[id].channelId);
                if (!channel) return message.channel.send(localization.rolemenu.chan_deleted).catch(console.error);
                let msg = await channel.messages.fetch(menus[id].msgId).catch(console.error);
                if (!msg) return message.channel.send(localization.rolemenu.msg_deleted).catch(console.error);


                try {
                    client.roleMenuCollectors.get(msg.id).stop()
                } catch (e) {
                    return message.channel.send(firstUpper(localization.general.error)+' !').catch(console.error)
                }

                message.channel.send(localization.rolemenu.add_rm).then(async (msg) => {

                    msg.react('➕').then(msg.react('➖'))

                    const filter = (r, user) => user.id === message.author.id;

                    const collector = msg.createReactionCollector( {filter,time: 2 * 60 * 1000})

                    collector.on('collect', async (r) => {

                        if (r.emoji.name === '➕') {
                            collector.stop()
                            msg.delete().catch(console.error)

                            message.channel.send(localization.rolemenu.add_txt).then((msg_) => {
                                const filter = (msg) => message.author.id === msg.author.id
                                const collector = message.channel.createMessageCollector({filter, time: 2 * 60 * 1000});
                                collector.on('collect', (msg) => {
                                    msg.delete()
                                    if (msg.content === 'exit') {
                                        collector.stop()
                                        msg_.delete()
                                    }
                                    let roles = [];
                                    let args = msg.content.trim().split(/\s+/)

                                    msg.mentions.roles.forEach((value, key) => {
                                        if (message.guild.roles.cache.get(key)) roles.push(key)
                                    });
                                    for (let i = 0; i < args.length; i++) {
                                        if (roles.indexOf(args[i]) === -1 && !args[i].startsWith("<") && message.guild.roles.cache.get(args[i])) roles.push(args[i])
                                    }

                                    if (!roles) return;
                                    for (let roleID of rolesMap )
                                    {
                                        let index = roles.indexOf(rolesMap.get(rolesMap[roleID]))
                                        if (index !== -1) {
                                            roles.splice(index, 1)
                                        }
                                    }

                                    (function setup_msg(index) {
                                        if (index === roles.length) return
                                        let role = message.guild.roles.cache.get(roles[index]);
                                        if (!role) setup_msg(index + 1);

                                        message.channel.send(localization.rolemenu.react+ role.name).then(
                                            async msg => {

                                                const filter = (r, user) => {
                                                    return user.id === message.author.id;
                                                }
                                                const collector = msg.createReactionCollector({filter, time: 2 * 60 * 1000})

                                                collector.on('collect', (r) => {

                                                    if (!rolesMap.hasOwnProperty(r.emoji.name)) {
                                                        rolesMap.set(r.emoji.name,role.id)
                                                        collector.stop();
                                                        msg.delete()
                                                        index++;
                                                        if (index === roles.length) return message.channel.send('ok').then((msg) => msg.delete({timeout: 5000}));
                                                        setup_msg(index)
                                                    }

                                                })
                                            }).catch(console.error)
                                    })(0)
                                })

                                collector.on('end', async () => {
                                    let rolesList = rolesString(rolesMap)
                                    let msg = await channel.messages.fetch(menus[id].msgId).catch(console.error);

                                    const embed = new Discord.MessageEmbed()
                                        .setColor(client.color)
                                        .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
                                        .addField(`Role Menu\n\u200b`, rolesList, true)

                                    if (msg.embeds[0].description) embed.setDescription(msg.embeds[0].description)
                                    if (msg.embeds[0].title) embed.setTitle(msg.embeds[0].title)
                                    msg.edit({embeds: [embed]}).catch(console.error)

                                    let kc = false;
                                    let fct = async (roleID,emoji_)=>{
                                        let emoji = client.emojis.cache.find(val => val.name === emoji_)
                                        if (emoji) {
                                            await msg.react(emoji).catch(console.error)
                                        } else {
                                            await msg.react(emoji_).catch(() => {
                                                message.channel.send(localization.rolemenu.no_react + emoji_).catch(console.error);
                                                kc = true;
                                                return message.channel.send(localization.rolemenu.kc).catch(console.error)
                                            })
                                        }
                                    }
                                    rolesMap.forEach(fct)
                                    if (kc) return;
                                    menus[id].rolesMap = rolesMap
                                    GuildRoleMenu.findOneAndUpdate({messageId:menus[id].messageId},{rolesMap:rolesMap}).exec()
                                    client.roleMenus.set(message.guild.id, menus)
                                    client.roleMenuCollectors.delete(msg.id)
                                    roleMenu(client, menus[id]).catch(console.error)
                                })

                            })


                        } else if (r.emoji.name === '➖') {
                            collector.stop()
                            msg.delete().catch(console.error)
                            let txt = rolesString(rolesMap)
                            message.channel.send(txt).then(async (msg) => {
                                let fct = async (roleID,emoji_)=>{
                                let emoji = client.emojis.cache.find(val => val.name === emoji_)
                                if (emoji) {
                                    await msg.react(emoji).catch(console.error)
                                } else {
                                    await msg.react(emoji_).catch(console.error)
                                }
                            }
                                rolesMap.forEach(fct)
                                await msg.react('❌').catch(console.error)

                                const filter = (r, user) => user.id === message.author.id;
                                const collector_ = msg.createReactionCollector(filter);
                                let msg_ = msg
                                collector_.on('collect', async (r) => {
                                    rolesMap.forEach((roleID,emoji)=>{
                                        if (emoji === r.emoji.name) {
                                            rolesMap.remove(r.emoji.name)
                                        }
                                    })

                                    if (r.emoji.name === '❌') {
                                        collector_.stop()
                                        msg_.delete()
                                        let size = rolesMap.size
                                        if (size === 0) {
                                            let msg = await channel.messages.fetch(menus[id].msgId).catch(console.error);
                                            if (msg) msg.delete().catch(console.error)
                                            menus.splice(id, 1)
                                            GuildRoleMenu.findOneAndDelete({messageId:menus[id].messageId}).exec()
                                            client.roleMenus.set(message.guild.id, menus)
                                            return message.channel.send(localization.rolemenu.deactivate)
                                        }

                                        let rolesList = rolesString(rolesMap)
                                        let msg = await channel.messages.fetch(menus[id].msgId).catch(console.error);

                                        const embed = new Discord.MessageEmbed()
                                            .setColor(client.color)
                                            .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
                                            .addField(`Role Menu\n\u200b`, rolesList, true)
                                        msg.edit({embeds: [embed]}).catch(console.error)
                                        msg.reactions.removeAll().catch(console.error)
                                        let fct = async (roleID,emoji_)=>{
                                            if (!msg.reactions.cache.find((reaction) => reaction.emoji.name === emoji_.name)) {
                                                let emoji = client.emojis.cache.find(val => val.name === emoji_)
                                                if (emoji) {
                                                    await msg.react(emoji).catch(console.error)
                                                } else {
                                                    await msg.react(emoji_).catch(console.error)
                                                }
                                            }
                                        }
                                        rolesMap.forEach(fct)

                                        GuildRoleMenu.findOneAndUpdate({messageId:menus[id].messageId},{rolesMap:rolesMap}).exec()
                                        client.roleMenus.set(message.guild.id, menus)
                                        client.roleMenuCollectors.delete(msg.id)
                                        roleMenu(client, menus[id]).catch(console.error)
                                    }

                                })
                            })


                        }

                    })
                }).catch(console.error)

            }


        } else if (args[0] === 'list') {

            let menus = client.roleMenus.get(message.guild.id) || [];
            const embed = new Discord.MessageEmbed()
                .setColor(client.color)
                .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                .setTitle('RolesMenu');

            if (args[1]) {
                let id = args[1];
                if (!menus[id]) return message.channel.send(localization.rolemenu.not_menu).catch(console.error)
                let rolesMap = menus[id].rolesMap;

                let guild = client.guilds.cache.get(menus[id].guildId);
                let channel = guild.channels.cache.get(menus[id].channelId);
                if (!channel) return message.channel.send(localization.rolemenu.chan_deleted).catch(console.error);
                let msg = await channel.messages.fetch(menus[id].msgId).catch(console.error);
                if (!msg) return message.channel.send(localization.rolemenu.msg_deleted).catch(console.error);
                let rolesList = rolesString(rolesMap);


                embed
                    .addField(localization.rolemenu.list+' ' + id, rolesList)

            } else {

                for (let i = 0; i < menus.length; i++) {
                    let deleted = false;
                    let guild = client.guilds.cache.get(menus[i].guildId);
                    if (!guild) return
                    let channel = guild.channels.cache.get(menus[i].channelId);
                    if (channel) {
                        let msg = await channel.messages.fetch(menus[i].msgId).catch(() => {
                            deleted = true
                        });
                        if (!msg) deleted = true;
                    } else {
                        deleted = true;
                    }

                    if (deleted) {
                        embed
                            .addField('Menu № ' + i, localization.rolemenu.not_active);
                    } else {
                        embed
                            .addField('Menu № ' + i, localization.rolemenu.in_chan + channel.name);
                    }

                }

            }
            return message.channel.send({embeds : [embed]})

        } else if (args[0] === 'remove') {
            if (isNaN(args[1])) return message.channel.send(localization.general.nan).catch(console.error);
            let menus = client.roleMenus.get(message.guild.id);
            let id = args[1];

            if (!menus[id]) return message.channel.send(localization.rolemenu.not_menu).catch(console.error);

            GuildRoleMenu.findOneAndDelete({messageId:menus[id].messageId}).exec()
            menus.splice(id, 1);


            client.roleMenus.set(message.guild.id, menus)

            return message.channel.send('Menu №' + id +' '+ localization.general.deleted).catch(console.error)
        } else {
            message.delete().catch(console.error)

            let roles = [];
            let rolesMap = new Map()

            let inv = args[0] === 'inv';
            if(inv) args.splice(0,1)
            let opt = args.join(' ').split(',')
            let title = opt[0] || ''
            let desc = opt[1] || ''

            message.channel.send(localization.rolemenu.add_txt).then((msg_) => {
                const filter = (msg) => msg.author.id === message.author.id;
                const collector = message.channel.createMessageCollector({filter, time: 6 * 60 * 1000});
                collector.on('collect', (msg) => {
                    let subRoles = [];
                    msg.delete()
                    if (msg.content === 'exit') {
                        collector.stop()
                        msg_.delete()
                        embed()
                    }

                    msg.mentions.roles.forEach((value, key) => {
                        if (message.guild.roles.cache.get(key) && roles.indexOf(key) === -1) subRoles.push(key)
                    });
                    for (let i = 0; i < args.length; i++) {
                        if (roles.indexOf(args[i]) === -1 && !args[i].startsWith("<") && message.guild.roles.cache.get(args[i]) && roles.indexOf(args[i]) === -1) subRoles.push(args[i])
                    }

                    if (!subRoles) return;


                    (function setup_msg(index) {
                        if (index === subRoles.length) return
                        let role = message.guild.roles.cache.get(subRoles[index]);
                        if (!role) setup_msg(index + 1);

                        message.channel.send(localization.rolemenu.react + role.name).then(
                            async msg => {

                                const filter = (r, user) => {
                                    return user.id === message.author.id;
                                }
                                const collector = msg.createReactionCollector({filter, time: 3 * 60 * 1000})

                                collector.on('collect', (r) => {

                                    if (!rolesMap.hasOwnProperty(r.emoji.name)) {
                                        rolesMap.set(r.emoji.name, role.id)
                                        roles.push(role.id)
                                        collector.stop();
                                        msg.delete()
                                        index++;
                                        msg_.edit(localization.rolemenu.add_txt+'\n'+rolesString(rolesMap))
                                        if (index === subRoles.length) return message.channel.send('ok').then((msg) => msg.delete({timeout: 5000}));
                                        setup_msg(index)
                                    }

                                })
                            }).catch(console.error)
                    })(0)
                })
            })

            function embed() {
                if (rolesMap.size <= 0) return message.channel.send(localization.general.no_role)
                let menus = client.roleMenus.get(message.guild.id);
                let rolesList = rolesString(rolesMap)

                const embed = new Discord.MessageEmbed()
                    .setColor(client.color)
                    .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
                    .addField(`Role Menu\n\u200b`, rolesList, true)

                if(title) embed.setTitle(title)
                if(desc) embed.setDescription(desc)

                message.channel.send({embeds : [embed]}).then(async msg => {
                    let kc = false;
                    let fct = async (roleID,emoji_)=>{
                        let emoji = client.emojis.cache.find(val => val.name === emoji_)
                        if (emoji) {
                            await msg.react(emoji).catch(console.error)
                        } else {
                            await msg.react(emoji_).catch(() => {
                                message.channel.send(localization.rolemenu.no_react + emoji_).catch(console.error);
                                kc = true;
                                return message.channel.send(localization.rolemenu.kc).catch(console.error)
                            })
                        }
                    }
                    rolesMap.forEach(fct)
                    if (kc) return;

                    const menu = await GuildRoleMenu.create({
                        rolesMap: rolesMap,
                        msgId: msg.id,
                        channelId: msg.channel.id,
                        guildId: msg.guild.id,
                        inv: inv
                    })
                    menus.push(menu);

                    roleMenu(client,menu)

                }).catch(console.error)

            }


        }

        function rolesString(rolesMap) {
            let rolesList = '';

            rolesMap.forEach((roleID,emoji_)=>{
                let role = message.guild.roles.cache.get(roleID)
                let emoji
                if (client.emojis.cache.find(val => val.name === emoji_)) {
                    emoji = client.emojis.cache.find(val => val.name === emoji_)
                } else {
                    emoji = emoji_
                }
                rolesList += emoji.toString() + " " + ": `" + role.name + "` \n"
            })

            return rolesList
        }
    },
    aliases: ['rlm', 'rolemenu'],
    description: {french: "Crée un menu de role.", english: 'Create a role menu.'},
    help: [
        {cmd: "rolemenu inv (optional) {title (optional)},{description (optional)}", desc: "rolemenu_desc_0"},
        {cmd: "rolemenu list {number (optional)}", desc: "rolemenu_desc_1"},
        {cmd: "rolemenu remove {number}", desc: "rolemenu_desc_2"},
        {cmd: "rolemenu modify {number}", desc: "rolemenu_desc_3"},
    ],
    info: {dm: false, name: 'rolemenu', category: "utils"}
}
