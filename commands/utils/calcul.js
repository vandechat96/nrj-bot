module.exports = {
    run: async (client, message, args) => {

        let argsOK = espace(args.join('')).split(/\s+/)
        if (argsOK[0] === ' ') argsOK.splice(0, 1)
        if (argsOK[argsOK.length - 1] === '') argsOK.pop()
        if (argsOK.length===0) return
        message.channel.send(`= ${calcul(argsOK)}`)

        function calcul(args) {
            let operation = 0;
            while (args.length !== 1) {
                let act = 0
                for (let i = 0; i < args.length; i++) {
                    if (isNaN(args[i])) {
                        switch (operation) {
                            case 0:
                                if (args[i] === '(') {
                                    act++
                                    let tab = []
                                    for (let j = i + 1; args[j] !== ')'; j++) {
                                        tab.push(args[j])
                                    }
                                    let len = tab.length
                                    let nbr = calcul(tab)
                                    args.splice(i, len + 1)
                                    args[i] = nbr
                                }


                                break;

                            case 1:
                                if (args[i] === '^') {
                                    act++
                                    let nbr = Math.pow(parseFloat(args[i - 1]), parseFloat(args[i + 1]));
                                    args.splice(i - 1, 2);
                                    args[i - 1] = nbr
                                }
                                break;

                            case 2:
                                if (args[i] === '*') {
                                    act++
                                    let nbr = parseFloat(args[i - 1]) * parseFloat(args[i + 1]);
                                    args.splice(i - 1, 2);
                                    args[i - 1] = nbr
                                }
                                if (args[i] === '/') {
                                    act++
                                    let nbr = parseFloat(args[i - 1]) / parseFloat(args[i + 1]);
                                    args.splice(i - 1, 2);
                                    args[i - 1] = nbr
                                }
                                break;

                            case 3:
                                if (args[i] === '+') {
                                    act++
                                    let nbr = parseFloat(args[i - 1]) + parseFloat(args[i + 1]);
                                    args.splice(i - 1, 2);
                                    args[i - 1] = nbr
                                }
                                if (args[i] === '-') {
                                    act++
                                    let nbr = parseFloat(args[i - 1]) - parseFloat(args[i + 1]);
                                    args.splice(i - 1, 2);
                                    args[i - 1] = nbr
                                }
                                break;
                        }

                    }
                }
                if (act === 0) operation++;
            }
            return args[0]
        }

        function espace(msg) {
            let f = ''
            let par = false
            for (let i = 0; i < msg.length; i++) {
                if (isNaN(msg[i])) {
                    if(msg[i] ==='(' && par) f += ' * '
                    f += " " + msg[i] + ' '
                    par = false
                } else {
                    f += msg[i]
                    par = true
                }
            }
            console.log(par)
            return f
        }
    },
    aliases: ['calc'],
    description: {french: "Calcul l'expression demandé.", english: 'Calculate the asked expression.'},
    help: [],
    info: {dm: false, name: 'calcul', category: "utils"}
}
