const {firstUpper} = require("../../utils/utils");
const {getFrDate, getEnDate} = require("../../utils/utils");

module.exports = {
    run: async (client, message, args, opt) => {
        const Discord = require('discord.js');
        let guild = message.guild
        const {config} = opt
        const language = config.language
        const localization = client.localization.get(language)
        const dateFct = language === "french" ? getFrDate : getEnDate
        const serverInfo = localization.server_info

        const botCount = guild.members.cache.filter(member => member.user.bot).size
        const catCount = guild.channels.cache.filter(channel => channel.type === 'category').size
        const txtCount = guild.channels.cache.filter(channel => channel.type === 'text').size
        const vocCount = guild.channels.cache.filter(channel => channel.type === 'voice').size
        const animEmojiCount = guild.emojis.cache.filter(emoji => emoji.animated).size

        let roles = ''
        guild.roles.cache.forEach(role => roles += role.name + ', ')
        roles = roles.slice(0, roles.length - 2)

        let joinedSince = new Date(Date.now() - guild.joinedAt)
        let createdSince = new Date(Date.now() - guild.createdAt)

        const embed = new Discord.MessageEmbed()
            .setTitle(serverInfo.title)
            .setColor(client.color)
            //.setDescription("Information du serveur")
            .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
            .setThumbnail(message.guild.iconURL())
            .setTimestamp()

            .addField(serverInfo.server_name, `\`\`\`${guild.name} (${guild.nameAcronym})\`\`\``, true)
            .addField(serverInfo.owner, `\`\`\`${guild.owner.user.username}\`\`\``, true)

            .addField(`${serverInfo.member_count} [${guild.memberCount}]`, `\`\`\`${firstUpper(localization.general.member)}s : ${guild.memberCount - botCount} | Bots : ${botCount} \`\`\``)

            .addField(serverInfo.afk_channel, `\`\`\`${guild.afkChannel ? guild.afkChannel.name : localization.general.undefined} (${guild.afkTimeout / 60} min.)\`\`\``, true)
            .addField(serverInfo.region, `\`\`\`${guild.region}\`\`\``, true)


            .addField(`${serverInfo.channel_count} [${guild.channels.cache.size}] `, `\`\`\`${firstUpper(localization.general.category)} : ${catCount} | ${firstUpper(localization.general.text)} : ${txtCount} | ${firstUpper(localization.general.voice)} : ${vocCount} \`\`\``)

            .addField(serverInfo.explicit_content_filter, `\`\`\`${guild.explicitContentFilter} \`\`\``, true)
            .addField(serverInfo.verification_level, `\`\`\`${guild.verificationLevel} \`\`\``, true)

            .addField(`${serverInfo.emojis} [${guild.emojis.cache.size}]`, `\`\`\`${firstUpper(localization.general.normal)} : ${guild.emojis.cache.size - animEmojiCount} | ${firstUpper(localization.general.animated)} : ${animEmojiCount} \`\`\``)

            .addField(serverInfo.system_channel, `\`\`\`${guild.systemChannel ? guild.systemChannel.name : localization.general.undefined}\`\`\``, true)
            .addField(serverInfo.boost, `\`\`\`Level ${guild.premiumSubscriptionCount < 2 ? 0 : (guild.premiumSubscriptionCount < 15 ? 1 : guild.premiumSubscriptionCount < 30 ? 2 : 3)} | ${guild.premiumSubscriptionCount} boosts\`\`\``, true)

            .addField(serverInfo.roles, `\`\`\`${roles}\`\`\``)

            .addField(serverInfo.created, `\`\`\`${dateFct(guild.createdAt)} (${serverInfo.since.replace("{day}", createdSince.getDate()).replace('{month}', createdSince.getMonth()).replace('{year}', createdSince.getFullYear() - 1970)})\`\`\``)

            .addField(serverInfo.joined, `\`\`\`${dateFct(guild.joinedAt)} (${serverInfo.since.replace("{day}", joinedSince.getDate()).replace('{month}', joinedSince.getMonth()).replace('{year}', joinedSince.getFullYear() - 1970)})\`\`\``)


        //.addField("afk timeout",guild.afkTimeout,true)
        //.addField("available",guild.available,true)
        //.addField("icon",guild.icon,true)
        //.addField("id",guild.id,true)
        //.addField("large",guild.large,true)


        //.addField("ownerID",guild.ownerID,true)
        //.addField("owner",guild.owner,true)
        //.addField("nameAcronym",guild.nameAcronym,true)

        //.addField("systemChannelID",guild.systemChannelID,true)

        //.addField("iconURL",guild.iconURL,true);

        message.channel.send({embeds : [embed]}).catch(console.error)
    },
    aliases: ['si', 'serverinfo'],
    description: {french: "Donne des informations sur le serveur", english: 'Give informations about the server'},
    help: [],
    info: {dm: false, name: 'servinfo', category: "utils"}
}
