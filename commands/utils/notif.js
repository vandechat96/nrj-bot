const {isAdmin} = require("../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        if (!isAdmin(message, config)) return message.reply(localization.general.no_perm).catch(console.error);

        if (!args[0]) return;

        const role = message.mentions.roles.first() || message.guild.roles.cache.get(args[0]);
        if (role) {
            let members = (await message.guild.members.fetch().catch(console.error)).filter((member)=>member.roles.cache.has(role.id))
            let txt = ''
            for (let member of members) {
                txt += member[1].toString()
            }
            if (txt) return message.channel.send(`${txt}`).catch(console.error)
        } else {
            return message.reply(localization.general.not_role).catch(console.error)
        }
    },
    aliases: ['notifie', "all","notify"],
    description: {french: "Notifie tout les membres d'un role.", english: 'Notify all the member of a role.'},
    help: [{cmd: "notif {role}", desc: "notif_desc_0"}],
    info: {dm: false, name: 'notif', category: "utils"}
}
