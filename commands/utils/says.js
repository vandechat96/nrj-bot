module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)
        const Discord = require('discord.js');
        const utils = require('../../utils/utils.js');
        const embed = new Discord.MessageEmbed()
            .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
            .setColor(client.color);

        args = message.content.slice(config.prefix.length + 5).split('&');

        if (args[1]) {
            embed
                .setTitle(args[0])
                .setDescription(args[1])
        } else {
            message.channel.send(localization.general.not_complete).catch(console.error)
        }

        if (utils.validURL(args[2])) {
            embed.setImage(args[2])
        } else if (args[2]) {
            message.channel.send(localization.general.invalid_url).catch(console.error)
        }

        message.channel.send({embeds : [embed]}).catch(console.error);
    },
    aliases: [],
    description: {french: "Crée un embed.", english: 'Create an embed'},
    help: [{cmd: "says {title}&{subtitle}&{image link (optional)}", desc: "says_desc_0"}],
    info: {dm: false, name: 'says', category: "utils"}
}
