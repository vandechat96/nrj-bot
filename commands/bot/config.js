const {isAdmin} = require("../../utils/utils");
const Discord = require('discord.js');
module.exports = {
    run: async (client, message, args, opt) => {
        const config = opt.config
        const localization = client.localization.get(config.language)
        if (!isAdmin(message, opt.config)) return message.reply(localization.general.no_perm || "no_perm");

        if (!args[0]) {

            const embed = new Discord.MessageEmbed()
                .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                .setColor(client.color)
                .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
                .setTimestamp()
                .addField(localization.config.cfg_server || "cfg_server", `${localization.config.prefix || "prefix"}: ${config.prefix}
                ${localization.config.log_channel || "log_channel"}: ${getFromGuild(config.logChannel)}
                ${localization.config.wc_channel || "wc_channel"}: ${getFromGuild(config.welcomeChannel)}
                ${localization.config.admin_role || "admin_role"}: ${config.adminRoles.reduce((acc,curr)=>acc+getFromGuild(curr)+' ',"")}
                ${localization.config.cooldown || "cooldown"}: ${config.cooldown} s
                ${localization.config.muted_role || "muted_role"}: ${getFromGuild(config.muteRole)}
                ${localization.config.base_role || "base_role"}: ${getFromGuild(config.baseRole)}
                ${localization.config.interserv_channel || "interserv_channel"}: ${getFromGuild(config.commChannel)}
                ${localization.config.language || "language"}: ${config.language}
                \n`, true);
            return message.channel.send({embeds : [embed]})

        } else {
            if (!args[1]) return;
            let role
            let channel
            switch (args[0]) {
                case "setPrefix":
                    config.prefix = args[1];
                    config.save()
                    message.reply(`${localization.config.prefix || "prefix"}  ${localization.general.changed || "changed"} ${config.prefix} !`).catch(console.error)
                    break
                case 'setCld':
                    if (!isNaN(args[1]) || args[1] < 0 || args[1] > 30) {
                        config.cooldown = args[1];
                        config.save()
                        message.reply(`${localization.config.cooldown || "cooldown"} ${localization.general.changed || "changed"} ${config.cooldown}s !`).catch(console.error)
                    } else {
                        message.reply(localization.general.nan || "nan").catch(console.error)
                    }
                    break
                /*
                case 'setRolePelo':
                    role = message.mentions.roles.first() || message.guild.roles.cache.get(args[1]);
                    if (role) {
                        config.rolePelo = role.id;
                        config.save()
                        message.reply(`${localization.config.platoon_role || "platoon_role"} ${localization.general.changed || "changed"} <@&${config.rolePelo}> !`).catch(console.error)
                    } else {
                        message.reply(localization.general.not_role || "not_role").catch(console.error)
                    }
                    break*/
                    case 'addRoleAdmin':
                    role = message.mentions.roles.first() || message.guild.roles.cache.get(args[1]);
                    if (role) {
                        if(config.adminRoles.indexOf(role.id) === -1) config.adminRoles.push(role.id);
                        config.save()
                        message.reply(`${localization.config.admin_role || "admin_role"} ${localization.general.changed || "changed"} ${config.adminRoles.reduce((acc,curr)=>acc+getFromGuild(curr)+' ',"")} !`).catch(console.error)
                    } else {
                        message.reply(localization.general.not_role || "not_role").catch(console.error)
                    }
                    break
                case 'removeRoleAdmin':
                    role = message.mentions.roles.first() || message.guild.roles.cache.get(args[1]);
                    if (role) {
                        let index = config.adminRoles.indexOf(role.id)
                        if(index !== -1) config.adminRoles.splice(index,1);
                        config.save()
                        message.reply(`${localization.config.admin_role || "admin_role"} ${localization.general.changed || "changed"} ${config.adminRoles.reduce((acc,curr)=>acc+getFromGuild(curr)+' ',"")} !`).catch(console.error)
                    } else {
                        message.reply(localization.general.not_role || "not_role").catch(console.error)
                    }
                    break
                case 'setLogChannel':
                    channel = message.mentions.channels.first() || client.channels.cache.get(args[1]);
                    if (channel) {
                        config.logChannel = channel.id;
                        config.save()
                        message.reply(` ${localization.config.log_channel || "log_channel"} ${localization.general.changed || "changed"} <#${config.logChannel}> !`).catch(console.error)
                    } else if (args[1] === 'null') {
                        config.logChannel = '0';
                        config.save()
                        message.reply(` ${localization.config.log_channel || "log_channel"} ${localization.general.changed || "changed"} <#${config.logChannel}> !`).catch(console.error)
                    } else {
                        message.reply(localization.general.not_channel || "not_channel").catch(console.error)
                    }
                    break
                case 'setBvnChannel':
                    channel = message.mentions.channels.first() || client.channels.cache.get(args[1]);
                    if (channel) {
                        config.welcomeChannel = channel.id;
                        config.save()
                        message.reply(` ${localization.config.wc_channel || "wc_channel"} ${localization.general.changed || "changed"} <#${config.welcomeChannel}> !`).catch(console.error)
                    } else if (args[1] === 'null') {
                        config.welcomeChannel = '0';
                        config.save()
                        message.reply(` ${localization.config.wc_channel || "wc_channel"} ${localization.general.changed || "changed"} <#${config.welcomeChannel}> !`).catch(console.error)
                    } else {
                        message.reply(localization.general.not_channel || "not_channel").catch(console.error)
                    }
                    break
                case 'setRoleMute':
                    role = message.mentions.roles.first() || message.guild.roles.cache.get(args[1]);
                    if (role) {
                        config.muteRole = role.id;
                        config.save()
                        message.reply(` ${localization.config.muted_role || "muted_role"} ${localization.general.changed || "changed"} <@&${config.muteRole}> !`).catch(console.error)
                    } else if (args[1] === 'null') {
                        config.muteRole = '0';
                        config.save()
                        message.reply(` ${localization.config.muted_role || "muted_role"} ${localization.general.changed || "changed"} <@&${config.muteRole}> !`).catch(console.error)
                    } else {
                        message.reply(localization.general.not_role || "not_role").catch(console.error)
                    }
                    break
                case 'setRoleBase':
                    role = message.mentions.roles.first() || message.guild.roles.cache.get(args[1]);
                    if (role) {
                        config.baseRole = role.id;
                        config.save()
                        message.reply(`${localization.config.base_role || "base_role"} ${localization.general.changed || "changed"} <@&${config.baseRole}> !`).catch(console.error)
                    } else {
                        message.reply(localization.general.not_role || "not_role").catch(console.error)
                    }
                    break
                case 'setCommChannel':
                    channel = message.mentions.channels.first() || client.channels.cache.get(args[1]);
                    if (channel) {
                        config.commChannel = channel.id;
                        config.save()
                        message.reply(`${localization.config.interserv_channel || "interserv_channel"} ${localization.general.changed || "changed"} <#${config.commChannel}> !`).catch(console.error)
                    } else if (args[1] === 'null') {
                        config.commChannel = '0';
                        config.save()
                        message.reply(`${localization.config.interserv_channel || "interserv_channel"} ${localization.general.changed || "changed"} null !`).catch(console.error)
                    } else {
                        message.reply(localization.general.not_channel || "not_channel").catch(console.error)
                    }
                    break
                case 'setLanguage':
                    if (client.localization.has(args[1])) {
                        config.language = args[1];
                        config.save()
                        message.reply(`${localization.config.language || "language"} ${localization.general.changed || "changed"} ${config.language} !`).catch(console.error)
                    } else {
                        message.reply(localization.general.lg_not_supported || "lg_not_supported").catch(console.error)
                    }
                    break
                default:
                    message.channel.send(localization.general.not_found)
            }
            client.servConfig.set(message.guild.id,config)
        }

        function getFromGuild(id) {
            if (!client.channels.cache.get(id)) {
                if (!message.guild.roles.cache.get(id)) {
                    return localization.general.undefined || "undefined"
                } else {
                    return `<@&${id}>`
                }
            } else {
                return `<#${id}>`
            }
        }
    },
    aliases: ['cfg'],
    description: {french: 'Configure les options du bot', english: 'Configure the bot options'},
    help: [{cmd: "config setPrefix {prefix}", desc: "config_desc_prefix"},
        {cmd: "config setCld {secondes}", desc: "config_desc_cld"},
        {cmd: "config setRolePelo {role}", desc: "config_desc_pelo"},
        {cmd: "config addRoleAdmin {role}", desc: "config_desc_admin_add"},
        {cmd: "config rmRoleAdmin {role}", desc: "config_desc_admin_rm"},
        {cmd: "config setRoleMute {role}", desc: "config_desc_mute"},
        {cmd: "config setRoleBase {role}", desc: "config_desc_base"},
        {cmd: "config setBvnChannel {channel}", desc: "config_desc_bvn"},
        {cmd: "config setLogChannel {channel}", desc: "config_desc_log"},
        {cmd: "config setCommChannel {channel}", desc: "config_desc_comm"},
        {cmd: "config setLanguage {language}", desc: "config_desc_language"},
    ],
    info: {dm: false, name: "config", category: "bot"}

}