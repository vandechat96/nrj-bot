const Discord = require('discord.js')
const {firstUpper} = require("../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        let config = opt.config || {}
        let prefix = config.prefix || ""
        let language = config.language || "french"
        const localization = client.localization.get(language)
        if (client.commands.has(args[0])) {
            let cmd = require(client.commands.get(args[0]))
            if (cmd.info.category === 'hidden' && cmd.info.name !== "help") return
            const embed = new Discord.MessageEmbed().setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                .setColor(client.color)
                .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
                .setTimestamp()
                .setTitle(`**__${firstUpper(cmd.info.name)}__**`)
                .setDescription(`${cmd.description[language]}\n ${cmd.aliases.length > 0 ? "Alias : " + cmd.aliases.toString() : ""}`)

            for (let sub_help of cmd.help) {
                embed.addField(`**${prefix}${sub_help.cmd}**`, `\`${localization[cmd.info.name][sub_help.desc]}\``)
            }
            message.channel.send({embeds : [embed]}).catch(console.error)
        } else {
            let modules = require('../../json/modules.json')
            let cmds = []
            let catEmoji = require('../../json/category_emoji.json')
            for (let module of modules) {
                if (!module.active) continue
                let cmdPath = client.commands.get(module.file)
                if (!cmdPath) continue
                let cmd = require(cmdPath)
                if (cmd.info.category !== 'hidden') cmds.push({file: cmd, category: cmd.info.category})
            }

            let embed = allCategoriesEmbed(catEmoji)

            message.channel.send({embeds : [embed]}).then(async (msg) => {
                for (let category in catEmoji) {
                    if (client.user.id === '719987677122003036' && (category === 'blitz') || category === 'nrj') continue
                    await msg.react(catEmoji[category])
                }
                let filter = (r, user) => {
                    return user.id === message.author.id;
                }
                let collector = new Discord.ReactionCollector(msg,  {filter, time: 600000})
                let back = false
                collector.on('collect', async (r) => {
                    let embed
                    if (back && r.emoji.name === "⬅️") {
                        embed = allCategoriesEmbed(catEmoji)
                        msg.edit({embeds : [embed]}).catch(console.error)
                        if(message.channel.type !== 'dm') msg.reactions.removeAll().catch(console.error)
                        back = false
                        for (let category in catEmoji) {
                            if (client.user.id === '719987677122003036' && (category === 'blitz') || category === 'nrj') continue
                            if (!back) await msg.react(catEmoji[category])
                        }
                        return
                    }
                    switch (r.emoji.name) {
                        /*case "nrj_logo":
                            embed = subCmdEmbed(cmds.filter((cmd) => {
                                return cmd.category === "nrj"
                            }))
                            break

                         */
                        case "blitz_logo":
                            embed = subCmdEmbed(cmds.filter((cmd) => {
                                return cmd.category === "blitz"
                            }))
                            break
                        case "⚒️":
                            embed = subCmdEmbed(cmds.filter((cmd) => {
                                return cmd.category === "mod"
                            }))
                            break
                        case "🎉":
                            embed = subCmdEmbed(cmds.filter((cmd) => {
                                return cmd.category === "fun"
                            }))
                            break
                        case "🎧":
                            embed = subCmdEmbed(cmds.filter((cmd) => {
                                return cmd.category === "music"
                            }))
                            break
                        case "🧰":
                            embed = subCmdEmbed(cmds.filter((cmd) => {
                                return cmd.category === "utils"
                            }))
                            break
                        case "🤖":
                            embed = subCmdEmbed(cmds.filter((cmd) => {
                                return cmd.category === "bot"
                            }))
                            break
                        case "🤷‍♀️":
                            embed = subCmdEmbed(cmds.filter((cmd) => {
                                return cmd.category === "other"
                            }))
                            break
                        default:
                            return
                    }
                    msg.edit({embeds : [embed]}).catch(console.error)
                    if(message.channel.type !== 'dm') msg.reactions.removeAll().catch(console.error)
                    await msg.react("⬅️")
                    back = true

                })
            })
        }

        function subCmdEmbed(cmds) {
            if (cmds.length <= 0) return NewEmbed()
            let embed = NewEmbed().setTitle(firstUpper(cmds[0].category))
            for (let cmd of cmds) {
                embed.addField(prefix + cmd.file.info.name, cmd.file.description[language])
            }
            return embed
        }

        function allCategoriesEmbed(catEmoji) {
            let embed = NewEmbed().setTitle("Aide")
            for (let category in catEmoji) {
                if (client.user.id === '719987677122003036' && (category === 'blitz') || category === 'nrj') continue
                let emoji = catEmoji[category].startsWith(':') ? `<${catEmoji[category]}>` : catEmoji[category]
                embed.addField(emoji + ": " + localization.help[category].title, localization.help[category].description)
            }
            return embed
        }

        function NewEmbed() {
            return new Discord.MessageEmbed()
                .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                .setColor(client.color)
                .setDescription(localization.help.help_desc_emb0 + "\n" + prefix + localization.help.help_desc_emb1)
                .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
                //.setThumbnail("")
                .setTimestamp()
        }

    },

    aliases: ['h', 'aide'],
    description: {french: 'Fournit de l\'aide sur les commandes du bot.', english: "Provides help to use the bot"},
    help: [{cmd: "help", desc: "help_desc_0"}, {cmd: "help {cmd}", desc: "help_desc_1"}],
    info: {dm: true, name: 'help', category: "hidden"}
}