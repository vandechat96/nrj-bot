module.exports = {
    run: async (client, message) => {

        message.channel.send("Ping?").then((msg) => {
            msg.edit(`Pong! Latency is ${msg.createdTimestamp - message.createdTimestamp}ms.`).catch(console.error);
        }).catch(console.error)

    },
    aliases: [],
    description: {french: 'Répond avec le ping du bot'},
    help: [],
    info: {dm: true, name: "ping", category: "bot"}
}
