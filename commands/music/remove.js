const Discord = require('discord.js')
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        let data = client.active.get(message.guild.id) || {};
        if (args[0] === 'last') args[0] = data.queue.length
        if (data.queue && !isNaN(args[0]) && --args[0] < data.queue.length && args[0] >= 0) {

            let song = data.queue[args[0]]
            message.channel.send(new Discord.MessageEmbed().addField(localization.remove.removed, song.txt)).catch(console.error)
            data.queue.splice(args[0], 1)
            client.active.set(message.guild.id, data);

            if (song.url === data.nowPlaying.url && data.queue.length>0) {
                await data.player.play(data.queue[data.queueIndex])
            }
        }
    },
    aliases: ['rm'],
    description: {french: "Supprime une piste de la queue.", english: 'Remove a track from the queue.'},
    help: [{cmd: "remove {nbr}", desc: "remove_desc_0"}],
    info: {dm: false, name: 'remove', category: "music"}
}
