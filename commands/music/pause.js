module.exports = { 
    run: async(client, message, args,opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        let data = client.active.get(message.guild.id) || {};

        if (!data.dispatcher) return message.channel.send("Il n'y a rien en lecture sur le serveur.").catch(console.error);

        data.dispatcher.pause()

        message.channel.send(localization.pause.paused).catch(console.error)
    }, 
   aliases: [],
   description: {french:"Pause la piste en lecture.",english:  'Pause the track playing.'},
   help:[],
   info:{dm:false,name:'pause',category:"music"}
}
