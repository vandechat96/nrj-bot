module.exports = {
    run: async (client, message, args) => {
        let data = client.active.get(message.guild.id) || {};
        if (!data.nowPlaying.type.startsWith("radio") && data.dispatcher && !isNaN(args[0]) && parseInt(args[0]) < data.nowPlaying.duration) {
            data.nowPlaying.seekTime = args[0]
            await data.player.play(data.queue[data.queueIndex],args[0])
        } else{
            console.log(parseInt(args[0]) < data.nowPlaying.duration)
        }
    },
    aliases: ['sk'],
    description: {french: "Fait un saut dans la piste.", english: 'Jump in the track.'},
    help: [{cmd: "seek {nbr}", desc: "seek_desc_0"}],
    info: {dm: false, name: 'seek', category: "music"}
}
