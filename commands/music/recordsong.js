const {menu} = require("../../utils/utils");

const {getList,getStations} = require("../../utils/recordlist");
module.exports = {
    run: async (client, message, args) => {
        const Discord = require('discord.js')
        if (args[0] === 'stations'){
            let list = await getStations()
            return menu(client,message,list,(elem)=>` [${elem}](http://history.radiorecord.ru/air/${elem}/)\n`,'Stations')
        }
        let list
        try {
            list = await getList(args[0] || 'hbass')
        } catch (e) {
            return message.channel.send(e)
        }

        if (args[1] && args[1] === 'list') {
            let title = 'Liste des chanssons'
            let format = song => `[${song.name}](http://${song.file})\n`;
            menu(client, message, list, format, title, true).catch(console.error)
        } else if (args[1] && args[1] === 'dl') {
            let song = list[list.length - 1]
            message.channel.send({
                files: ['http://' + song.file]
            }).catch(console.error)
        } else {
            let song = list[list.length - 1]
            let embed = newEmbed().setDescription(`[${song.name}](http://${song.file})\n`)
            message.channel.send({embeds : [embed]}).catch(console.error)
        }


        function newEmbed() {
            return new Discord.MessageEmbed()
                .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                .setColor(client.color)
                .setTitle("__**Now playing**__")
        }
    },
    aliases: ['rs'],
    description: {french: "Utilitaire pour les radios de Radio Record.", english: 'Utility for Radio Record\'s radio.'},
    help: [{cmd: "recordsong {station}", desc: "recordsong_desc_0"},
        {cmd: "recordsong {station} list", desc: "recordsong_desc_1"},
        {cmd: "recordsong {station} dl", desc: "recordsong_desc_2"},
        {cmd: "recordsong stations", desc: "recordsong_desc_3"}
        ],
    info: {dm: false, name: 'recordsong', category: "music"}
}
