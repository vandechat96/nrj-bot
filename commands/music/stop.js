const {isAdmin} = require("../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        let vc = message.guild.me.voice.channel

        if (!vc) {
            client.active.set(message.guild.id, undefined);
            return message.channel.send(localization.music.not_playing).catch(console.error);
        }

        if (!isAdmin(message, config) && !message.member.hasPermission("MOVE_MEMBERS") && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.reply(localization.general.no_perm || "no_perm");

        message.guild.me.voice.channel.leave()
        client.active.set(message.guild.id, undefined);
        return message.reply(localization.music.leave).catch(console.error);

    },
    aliases: ['stp'],
    description: {french: "Quite le salon vocal courant.", english: 'Quite the current voice channel.'},
    help: [],
    info: {dm: false, name: 'stop', category: "music"}
}
