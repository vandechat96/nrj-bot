const nowplaying = require("./nowplaying");
const {Song} = require("../../models/Song");
const {Player} = require("../../models/Player");
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        const Discord = require('discord.js');
        const fs = require("fs");
        let radio = JSON.parse(fs.readFileSync("./json/radio.json", "utf8"));
        let url = ' ';

        let active = client.active
        let data = active.get(message.guild.id) || {};
        if (data.dispatcher) return message.channel.send(localization.music.already_playing)
        data.guildID = message.guild.id;
        if (!data.voiceChannel && message.member.voice.channel) data.voiceChannel = message.member.voice.channel.id;
        let voiceChannel = message.member.guild.channels.cache.get(data.voiceChannel);

        if (!args[0]) {
            const embed = new Discord.MessageEmbed()
                .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                .setColor(client.color);
            let listeR = '';
            for (let i = 0; i < radio.length; i++) {
                listeR += `**${radio[i].name}** (${radio[i].cmd})\n`
            }
            embed.addField("__**" + localization.radio.list + " :**__", listeR)
            return message.channel.send({embeds : [embed]}).catch(console.error);
        } else {
            let index = radio.findIndex((obj) => obj.cmd === args[0])
            if (index === -1) return message.channel.send('radio non valide')

            if (!message.member.voice.channel) return message.channel.send(localization.music.not_voc)

            let type = url.includes('radiorecord') ? "radioRecord" : "radio"

            data.nowPlaying = new Song(
                "Now playing: " + radio[index].name + "\nUrl: " + url,
                radio[index].url,
                type,
                message.channel.id,
                radio[index].name,
                message.author.id,
                Infinity
            )

            if(!data.player) data.player = new Player(client, data, voiceChannel)

            await data.player.add(data.nowPlaying)

            active.set(message.guild.id, data)
        }

    },
    aliases: ['rad'],
    description: {french: "Peut lire différentes stations radio.", english: 'Can stream multiple radio stations.'},
    help: [{cmd: "radio", desc: "radio_desc_0"},
        {cmd: "radio {radio}", desc: "radio_desc_1"}],
    info: {dm: false, name: 'radio', category: "music"}
}
