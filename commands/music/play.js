const ytdl = require('ytdl-core-discord');
const yts = require('yt-search')
const {Player} = require("../../models/Player");
const {validURL} = require("../../utils/utils");
const {Song} = require('../../models/Song')
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        let active = client.active
        let data = active.get(message.guild.id) || {};
        data.guildID = message.guild.id;
        if (!data.voiceChannel && message.member.voice.channel) data.voiceChannel = message.member.voice.channel.id;
        let voiceChannel = message.member.guild.channels.cache.get(data.voiceChannel);

        if (!message.member.voice.channel) return message.reply(localization.music.not_voc).catch(console.error);

        if (!args[0] && data.connection) return resume()

        if (!args[0] && !message.attachments.first()) return message.reply(localization.general.not_complete).catch(console.error);

        let validate = args[0] ? await ytdl.validateURL(args[0]) : false

        if (validate) {
            await addYT(args[0])

        } else if (message.attachments.first()) {
            await addOther(message.attachments.first().url)

        } else if (validURL(args[0])) {
            await addOther(args[0])

        } else {
            const r = await yts(args.join(" "))
            const videos = r.videos
            if (videos.length > 0) await addYT(videos[0].url)
        }

        async function addYT(url) {
            let info
            try {
                info = await ytdl.getInfo(url)
            } catch (e) {
                console.error(e)
                return message.channel.send(localization.play.not_available).catch(console.error)
            }
            let song = new Song(
                `[${info.videoDetails.title}](${url}) - ${info.videoDetails.author.name}`,
                url,
                'yt',
                message.channel.id,
                info.videoDetails.title,
                message.author.id,
                info.videoDetails.lengthSeconds
            )
            await play(song)
        }

        async function addOther(url) {
            let song = new Song(
                url,
                url,
                'other',
                message.channel.id,
                undefined,
                message.author.id,
                undefined
            )
            await play(song)

        }

        async function play(song) {

            if(!data.player) data.player = new Player(client, data, voiceChannel)
            await data.player.add(song)
            active.set(message.guild.id, data)

        }

        async function resume() {
            if (data.dispatcher && data.dispatcher.paused) {
                data.dispatcher.resume()
                message.channel.send(localization.play.resumed)
            } else if (!data.dispatcher) {
                await play(data.queue[0])
            }
        }

        active.set(message.guild.id, data);
    },
    aliases: ['p'],
    description: {french: "Lit different flux audio.", english: 'Play different audio streams.'},
    help: [
        {cmd: "p", desc: "play_desc_0"},
        {cmd: "p {query | url}", desc: "play_desc_1"}
    ],
    info: {dm: false, name: 'play', category: "music"}

}