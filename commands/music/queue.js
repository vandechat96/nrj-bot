const Discord = require('discord.js')
const {toHMS} = require('../../utils/utils')
module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        let data = client.active.get(message.guild.id) || {};
        if (data.queue) {
            let tab = ''
            let i = 1
            for (let song of data.queue) {
                let duration = song.duration
                duration = duration === Infinity ? '∞' : isNaN(duration) ? 'NaN' : toHMS(duration)

                if (song === data.nowPlaying) tab += '=> '
                tab += `\`${i++}\` ¤ ${song.txt} | ${duration} | ${client.users.cache.get(song.userID).username}\n\n`

            }
            let embed = new Discord.MessageEmbed()
                .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
                .setColor(client.color)
                .setTitle("__**Queue**__")
                .setDescription(tab)

            message.channel.send({embeds : [embed]}).catch(console.error)
        } else {
            message.channel.send(localization.queue.empty).catch(console.error)
        }

    },
    aliases: ['q'],
    description: {french: "Montre la queue.", english: 'Display the queue.'},
    help: [],
    info: {dm: false, name: 'queue', category: "music"}
}
