module.exports = {
    run: async (client, message, args) => {
        let data = client.active.get(message.guild.id);

        if (!data || !data.dispatcher) return message.channel.send('Pas de musique en cours').catch(console.error);

        if (isNaN(args[0])) {
            let volume = data.volume || 20;
            return reactMsg(volume)
        } else if (args[0] > 200 || args[0] < 0) {
            return message.channel.send('Entrez un nombre entre 0-200 pour le volume. (0 muet - 200 maximum) ').catch(console.error)
        } else {
            data.volume = parseInt(args[0]);
            data.dispatcher.setVolume(args[0] / 100)
            return message.channel.send('Volume : ' + args[0]).catch(console.error)
        }

        async function reactMsg(volume) {
            message.channel.send('Volume : ' + volume).then(
                async function msg(msg) {

                    await msg.react('🔽').catch(console.error);
                    await msg.react('🔼').catch(console.error);

                    const filter = (r, user) => message.author.id === user.id
                    const collector = msg.createReactionCollector({filter, time: 90000, dispose: true});
                    let react = r => {
                        if (r.emoji.name === '🔼') {
                            if (volume >= 200) return;
                            volume += 5;
                            data.volume = volume;
                            data.dispatcher.setVolume(volume / 100)
                            msg.edit('Volume : ' + volume).catch(console.error)
                        }

                        if (r.emoji.name === '🔽') {
                            if (volume <= 0) return;
                            volume -= 5;
                            data.volume = volume;
                            data.dispatcher.setVolume(volume / 100)
                            msg.edit('Volume : ' + volume).catch(console.error)
                        }
                    }
                    collector.on('collect', react)
                    collector.on('remove', react)
                }).catch(console.error)
        }
    },
    aliases: ['vol', 'vlm'],
    description: {
        french: "Change le volume de la lecture en cour.",
        english: 'Change the volume of the current stream.'
    },
    help: [
        {cmd: "volume", desc: "volume_desc_0"},
        {cmd: "volume {volume}", desc: "volume_desc_1"}
    ],
    info: {dm: false, name: 'volume', category: "music"}
}
