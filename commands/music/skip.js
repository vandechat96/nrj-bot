module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        let data = client.active.get(message.guild.id) || {};
        let nbr = isNaN(args[0]) ? data.queueIndex + 1 : args[0] - 1

        if (data.queue && nbr >= 0) {
            if (nbr >= data.queue.length) {
                if (data.dispatcher) {
                    data.dispatcher.end()
                    message.channel.send(localization.skip.skipped)
                }

            } else {
                data.queueIndex = nbr
                await data.player.play(data.queue[data.queueIndex])
                //await play.run(client, message, ['skip__', data.queue[data.queueIndex]], opt)
                message.channel.send(localization.skip.skipped)
            }

        } else {
            message.channel.send(localization.music.not_playing)
        }
    },
    aliases: ['sk'],
    description: {french: "Saute une chanson dans la queue.", english: 'Skip a song in the queue'},
    help: [{cmd: "skip", desc: "skip_desc_0"},
        {cmd: "skip {nbr}", desc: "skip_desc_1"}],
    info: {dm: false, name: 'skip', category: "music"}
}
