const {toHMS} = require("../../utils/utils");
const {getList} = require("../../utils/recordlist");
const {getMetadata} = require("../../utils/utils");

module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        let data = client.active.get(message.guild.id) || {};

        let song = data.nowPlaying;


        if (!data.dispatcher) return message.channel.send(localization.music.not_playing).catch(console.error);


        await module.exports.setMetadata(song)


        let time = 'NaN'
        if (!isNaN(song.duration)) {
            let ecoule = Math.round((data.dispatcher.streamTime / 1000) + parseInt(song.seekTime || 0))
            let diff = song.duration - ecoule
            let petitPt = Math.round(ecoule / song.duration * 20)
            if (petitPt <= 20) {
                let bar = '[' + '▬'.repeat(petitPt) + '🔵' + '▬'.repeat(20 - petitPt) + ']'
                if (song.duration !== Infinity) time = bar + toHMS(ecoule) + ' / ' + toHMS(song.duration) + '\nRemaining :' + toHMS(diff)
                else time = bar + toHMS(ecoule) + ' / ∞'
            }

        }

        const embed = module.exports.baseEmbed(client,localization,song)

        embed.addField('\u200b', time)
            .setTitle("__**"+localization.nowplaying.np+"**__")
        return message.channel.send({embeds : [embed]}).catch(console.error)
    },
    aliases: ['np'],
    description: {french: "Montre la piste ou radio en cours.", english: 'See the current song or station playing.'},
    help: [],
    info: {dm: false, name: 'nowplaying', category: "music"},

    setMetadata : async function (song){
        if (song.type === "radioRecord") {
            let station = song.url.split("/")[3].split("_")[0]
            let list
            try {
                list = await getList(station || 'hbass')
            } catch (e) {
                return console.error(e)
            }
            let song = list[list.length - 1]
            song.txt = `[**${song.name}**](https://${song.file}) | [${song.name}](${song.url})`
        } else if (song.type !== 'yt') {

            let metadata
            try {
                metadata = await getMetadata(song.url)
            } catch (e){
                return e
            }


            song.txt = `[${song.name || 'unknown title'}](${song.url})`
            song.duration = metadata.duration ? Math.round(metadata.duration) : Infinity

            if (metadata.hasOwnProperty('tags')) {
                let tags = metadata.tags
                let title = tags.StreamTitle
                if (title) song.txt = `**${title}** | [${song.name || tags['icy-name'] || metadata.filename}](${song.url})`
                else {
                    song.name = tags.title + ' - ' + tags.artist
                    song.txt = `[${song.name || metadata.filename}](${song.url})`
                }
            }
        }

    },

    baseEmbed : function (client,localization,song){
        const Discord = require('discord.js')
        return new Discord.MessageEmbed()
            .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
            .setColor(client.color)
            .setDescription(song.txt)
            .setFooter(`${localization.nowplaying.requested} ${client.users.cache.get(song.userID).username} `)
    }
}
