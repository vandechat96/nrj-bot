const yts = require('yt-search')
const {Song} = require("../../models/Song");
const {menu} = require("../../utils/utils");
const {Player} = require("../../models/Player");

module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        if (args.length === 0) return message.channel.send(localization.general.not_complete)

        const r = await yts(args.join(" "))
        const videos = r.videos
        await menu(client, message, videos, (data, tour) => `${tour} ¤ **${data.title}** | ${data.author.name} | ${data.duration.seconds === 0 ? '∞' : data.duration.timestamp}\n `, localization.search.results)


        const filter = (msg) => {
            return msg.author.id === message.author.id
        };
        const collector = message.channel.createMessageCollector({filter, time: 2 * 60 * 1000});

        collector.on('collect', async (msg) => {
            if (msg.content.includes('search')||msg.content.includes('s')) return collector.stop()
            if (!isNaN(msg.content) && msg.content <= videos.length) {
                if (!message.member.voice.channel) return message.channel.send(localization.music.not_voc).catch(console.error)
                collector.stop()

                let data = client.active.get(message.guild.id) || {};
                if (!data.player) data.player = new Player(client, data, message.member.voice.channel)

                const video = videos[parseInt(msg.content) - 1];

                const song = new Song(
                    `[${video.title}](${video.url}) - ${video.author.name}`,
                    video.url,
                    'yt',
                    message.channel.id,
                    video.title,
                    message.author.id,
                    video.seconds
                )

                await data.player.add(song)

                client.active.set(message.guild.id, data)
            }
        })

    },
    aliases: ['sch','s'],
    description: {french: "Recherche sur youtube.", english: 'Search on youtube.'},
    help: [{cmd: "search {query}", desc: "search_desc_0"}],
    info: {dm: false, name: 'search', category: "music"}
}
