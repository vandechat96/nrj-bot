module.exports = {
    run: async (client, message, args, opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        let data = client.active.get(message.guild.id) || {};

        if (args[0] === 'track') data.loop = 0
        else if (args[0] === 'queue') data.loop = -1


        if (isNaN(data.loop)) data.loop = 0
        else if (data.loop >= 1) data.loop = -1
        else data.loop++
        client.active.set(message.guild.id, data)
        switch (data.loop) {
            case -1:
                message.channel.send(':x: ' + localization.loop.no_loop)
                break
            case 0:
                message.channel.send(':repeat: ' + localization.loop.queue_loop)
                break
            case 1:
                message.channel.send(':repeat_one: ' + localization.loop.track_loop)
                break
        }
    },
    aliases: ['lp'],
    description: {french: "Boucle la piste ou la queue.", english: 'Loop the track or the queue.'},
    help: [],
    info: {dm: false, name: 'loop', category: "music"}
}
