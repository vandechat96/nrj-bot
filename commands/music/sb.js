module.exports = { 
    run: async(client, message, args,opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        const soundboard = require('../../json/soundboard.json')
        const fs = require("fs");
        const configj = require("../../json/config.json");

        if (args[0] === 'add' && message.author.id === configj.ownerId && message.attachments.first() && args[1]) {
            let id = soundboard.length;
            soundboard[id] = {
                name: args[1],
                url: message.attachments.array()[0].url,
            };
            fs.writeFile("./json/soundboard.json", JSON.stringify(soundboard), (err) => {
                if (err) console.error(err)
            })
        } else if (args[0] === "list") {
            const Discord = require('discord.js');
            const embed = new Discord.MessageEmbed()
                .setAuthor(client.user.username,client.user.avatarURL())
                .setColor(16564228);
            let listeR = '';
            for (let i = 0; i < soundboard.length; i++) {
                listeR+=`**${soundboard[i].name}**\n`

            }
            embed.addField("__**Liste des sons disponibles :**__",listeR)
            return message.channel.send({embeds : [embed]}).catch(console.error);
        } else {
            if (!args[0]) return message.channel.send(localization.general.not_complete)
            if (!message.member.voice.channel) return  message.reply(localization.music.not_voc).catch(console.error);
            let data = client.active.get(message.guild.id);
            if(data) return message.channel.send(localization.music.already_playing)
            let index = soundboard.findIndex((obj)=>obj.name===args[0])
            let url = soundboard[index].url
            message.member.voice.channel
                .join()
                .then(async (connection) => {
                    let dispatcher = connection.play(url);

                    dispatcher.on("finish", () => {
                        //active.set(message.guild.id, undefined)
                        if (message.guild.me.voice.channel) message.guild.me.voice.channel.leave()
                    })
                })
                .catch(console.error);

        }

    }, 
   aliases: ['soundboard'],
   description: {french:"Joue un son de la banque de son.",english:  'Play a sound of the sound bank.'},
   help:[{cmd:"sb {list}",desc:"sb_desc_0"},
       {cmd:"sb {nom}",desc:"sb_desc_1"}],
   info:{dm:false,name:'sb',category:"music"}
}
