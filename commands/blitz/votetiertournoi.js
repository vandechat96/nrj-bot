const {isAdmin} = require("../../utils/utils");
module.exports = {
    run: async(client, message, args,opt) => {
        const Discord = require('discord.js');
        message.delete();

        let config = opt.config
        const localization = client.localization.get(config.language)

        if (!isAdmin(message, opt.config)) return message.reply(localization.general.no_perm || "no_perm");

        const embed = new Discord.MessageEmbed()
            //.setAuthor("NRJBot",client.user.avatarURL)
            .setColor(16564228)
            .setTimestamp()
            .setTitle('__VOTE POUR LE PROCHAIN TOURNOI !__')
            .setImage("https://cdn.discordapp.com/attachments/727923120354230416/727923338676273162/VOTE_TIER_TOURNOIS.png");

        message.channel.send({embeds : [embed]}).then( async function msg (msg){
            try {
                await msg.react('6⃣');
                await msg.react('8⃣');
                await msg.react('🔟')
            } catch (error) {
                console.error(error);
            }
        }).catch(console.error);
        message.channel.send("Merci de voter qu'une seule fois merci !").catch(console.error)
    }, 
   aliases: ['vtt'],
   description: {french:"Message de vote du tier tu prochain tournoi.",english:  'Vote message for the tier of the next tournament'},
   help:[],
   info:{dm:false,name:'votetiertournoi',category:"blitz"}
}
