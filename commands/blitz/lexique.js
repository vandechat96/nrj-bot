const {menu} = require("../../utils/utils");
module.exports = {
    run: async(client, message, args,opt) => {
        const {config} = opt
        const Discord = require('discord.js');
        const fs = require('fs');
        let lexique = JSON.parse(fs.readFileSync("./json/lexique.json", "utf8"));
        const configj = require("../../json/config.json");


        let letters=[{emoji:'🇦',char:'a'},{emoji:'🇧',char:'b'},{emoji:'🇨',char:'c'},{emoji:'🇩',char:'d'},{emoji:'🇪',char:'e'},{emoji:'🇫',char:'f'},{emoji:'🇬',char:'g'},{emoji:'🇭',char:'h'},{emoji:'🇮',char:'i'},{emoji:'🇯',char:'j'},{emoji:'🇰',char:'k'},{emoji:'🇱',char:'l'},{emoji:'🇲',char:'m'},{emoji:'🇳',char:'n'},{emoji:'🇴',char:'o'},{emoji:'🇵',char:'p'},{emoji:'🇶',char:'q'},{emoji:'🇷',char:'r'},{emoji:'🇸',char:'s'},{emoji:'🇹',char:'t'},{emoji:'🇺',char:'u'},{emoji:'🇻',char:'v'},{emoji:'🇼',char:'w'},{emoji:'🇽',char:'x'},{emoji:'🇾',char:'y'},{emoji:'🇿',char:'è'}];

        if(!args[0]) {
            let embed =  new Discord.MessageEmbed()
                .setAuthor(client.user.username,client.user.avatarURL())
                .setColor(client.color)
                .setFooter("© "+client.user.username,client.user.avatarURL())
                .setTimestamp()
                .setTitle('Lexique')
                .setDescription('Choisissez une lettre');

            message.channel.send({embeds : [embed]}).then (
                async function msg (msg){

                    let min = 0;
                    let max = 10;

                    await react(msg,min,max);


                    const filter = (r,user) => user.id === message.author.id;
                    const collector =  msg.createReactionCollector(filter, { time: 90000 })
                    collector.on('collect', async r =>{


                        if (r.emoji.name === '⬅') {
                            if (min === 0) return;
                            min-=10;
                            max-=10;
                            await msg.reactions.removeAll().catch(console.error);
                            await react(msg,min,max).catch(console.error)
                        } else if (r.emoji.name === '➡') {
                            min+=10;
                            max+=10;
                            await msg.reactions.removeAll().catch(console.error);
                            await react(msg,min,max).catch(console.error)
                        }

                        let index = letters.findIndex((data)=>{ return data.emoji.name === r.emoji.name});
                        if(index !== -1){
                            lexique = filterLetter(lexique,letters[index].char);

                            msg.delete().catch(console.error);
                            if (lexique.length === 0) return message.channel.send('Aucun mot dans le lexique !');
                            menu(client,message,lexique,(data)=>`${data.name} : \`${data.description}\`\n`,"Lexique").catch(console.error)
                        }


                    })
                }).catch(console.error)

        } else if (args[0] === "add"){
            if (message.author.id !== configj.ownerId) return
            let args =  message.content.slice(config.prefix.length+'lexique add'.length).trim().split(',');

            if(!args[1]) message.channel.send('Commande incomplète !').catch(console.error);


            lexique.push({name:args[0],description:args[1]});

            lexique.sort((a, b) => a.name > b.name ? 1 : -1);

            fs.writeFile("./json/lexique.json", JSON.stringify(lexique), (err) => {
                if (err) console.error(err)
            })


        } else if (args[0] === "remove"){
            if (message.author.id !== configj.ownerId) return
            let args =  message.content.slice(config.prefix.length+'lexique add'.length).trim().split(',');

            let index = lexique.findIndex((data)=>{ return data.name === args[0]});

            if(index === -1) return message.channel.send('Aucun mot dans le lexique !');

            lexique.splice(index,1);

            fs.writeFile("./json/lexique.json", JSON.stringify(lexique), (err) => {
                if (err) console.error(err)
            })


        }




        async function react(msg,min,max){
            await msg.react('%E2%AC%85');
            await msg.react('%E2%9E%A1');

            for (let i = min;i<max;i++){
                if (!letters[i]) break;
                await msg.react(letters[i].emoji)
            }
        }
        function filterLetter(lexique,letter){

            let result = [];

            for (let data of lexique) {
                if (data.name.toLowerCase().startsWith(letter)) {
                    result.push(data);
                }
            }

            return result
        }

    }, 
   aliases: [], 
   description: {french:"Petit lexique des mot utiliser sur wotb.",english:  'Lexicon of words used in wotb (no english yet sry).'},
   help:[],
   info:{dm:false,name:'lexique',category:"blitz"}
}
