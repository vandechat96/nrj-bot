module.exports = { 
    run: async(client, message, args,opt) => {
        let {config} = opt
         require(client.commands.get('tournoi')).run(client,message,args,{config,type:"training"}).catch(console.error)
    },
   aliases: [], 
   description: {french:"Vote pour un training inter-clan.",english:  'Vote for an inter-clan training.'},
   help:[{cmd:"training {tier} {heure}h{minutes} {jours} {nom du clan adverse} {[nom de la salle]} {code}(optional)",desc:"training_desc_0"}],
   info:{dm:false,name:'training',category:"blitz"}
}
