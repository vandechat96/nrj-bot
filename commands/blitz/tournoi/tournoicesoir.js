module.exports = { 
    run: async(client, message, args,opt) => {
        let {config} = opt
        args.splice(2,0,0)
         require(client.commands.get('tournoi')).run(client,message,args,{config,type:"tournoi"}).catch(console.error)
    }, 
   aliases: ['tcs'],
   description: {french:"Vote de tournoi pour le soir même.",english:  'Vote for tonight\'s tournament.'},
   help:[{cmd:"tournoiCeSoir {tier} {hours}h{minutes} {team name}",desc:"tournoicesoir_desc_0"}],
   info:{dm:false,name:'tournoicesoir',category:"hidden"}
}
