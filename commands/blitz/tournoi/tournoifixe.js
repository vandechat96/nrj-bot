module.exports = { 
    run: async(client, message, args,opt) => {
        let {config} = opt
        args.splice(1,0,'20h')
         require(client.commands.get('tournoi')).run(client,message,args,{config,type:"tournoi fixe"}).catch(console.error)
    }, 
   aliases: ['tournoiheurefixe'],
   description: {french:"Vote pour un tournoi fixe.",english:  'Vote for a fixed tournament.'},
   help:[{cmd:"tournoifixe {tier} {days} {team name}",desc:"tournoifixe_desc_0"}],
   info:{dm:false,name:'tournoifixe',category:"blitz"}
}
