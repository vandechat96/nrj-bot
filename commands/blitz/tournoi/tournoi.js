const {TournoiManager} = require("../../../models/TournoiManager");
const {isAdmin} = require("../../../utils/utils");
module.exports = {
    run: async(client, message, args,opt) => {
        const {config,type} = opt
        const localization = client.localization.get(config.language)
        if (!isAdmin(message, config)) return message.channel.send(localization.general.no_perm || "no_perm");
	    message.delete().catch(()=>console.log('no perm supp msg'))
        let manager
        try {
            manager = await new TournoiManager(client,type||'tournoi',args,{guild:message.guild,channel:message.channel,author:message.author,config})
            manager.start()
        } catch (e){
            console.error('mauvaise heure tkt')
        }


    }, 
   aliases: ['tournament'],
   description: {french:"Vote pour un futur tournoi.",english:  'Vote for an upcoming tournament.'},
   help:[{cmd:"tournoi {tier} {hours}h{minutes} {days} {team name}",desc:"tournoi_desc_0"}],
   info:{dm:false,name:'tournoi',category:"blitz"},
}
