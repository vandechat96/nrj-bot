module.exports = { 
    run: async(client, message, args,opt) => {
        let {config} = opt
        require(client.commands.get('tournoi')).run(client,message,args, {config,type:"entrainement"}).catch(console.error)
    }, 
   aliases: [], 
   description: {french:"Vote pour un entrainement dans le clan.",english:  'Vote for a training in the clan.'},
   help:[{cmd:'entrainement {tier} {hours}h{minutes} {days} {[room name]} {code}(optional)',desc:'entrainement_desc_0'}],
   info:{dm:false,name:'entrainement',category:"blitz"}
}
