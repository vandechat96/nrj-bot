module.exports = { 
    run: async(client, message, args) => {
        if (!args[0]) return
        switch (args[0].toLowerCase()){
            case "drive":
                message.channel.send('https://drive.google.com/drive/folders/0B7qdigCd3AFWenB6bEVGVnMzbVk').catch(console.error)
                break
            case "yt":
            case "youtube":
                message.channel.send('https://www.youtube.com/channel/UCvEvN7U7VbXYZrHuvepjWQQ').catch(console.error)
                break
            case "timify":
                message.channel.send('https://www.timify.com/fr-fr/profile/ride-of-tank-nrj-2').catch(console.error)
                break
            case "twitch":
                message.channel.send('https://www.twitch.tv/ride_of_tank').catch(console.error)
                break
            case "guilded":
                message.channel.send('http://www.guilded.gg/r/dcMldwD7RQ?i=6AXBgjzd').catch(console.error)
                break
            case "tipee":
                message.channel.send('https://fr.tipeee.com/rideoftank-nrj').catch(console.error)
                break
            case 'site':
                message.channel.send('https://lestankistesnrj.wixsite.com/clan-ride-of-tanknrj/').catch(console.error)
                break
        }
    }, 
   aliases: ['link','liens','liens'],
   description: {french:"Liens sociaux du clan NRJ.\nlinks [yt, twitch, tipeee, site]",english:  'NRJ\'s clan social links. \nlinks [yt, twitch,  tipee, site]'},
   help:[],
   info:{dm:false,name:'links',category:"blitz"}
}
