const {getRandomColor,isAdmin} = require("../../utils/utils");
module.exports = {
    run: async (client, message, args,opt) => {
        const {config} = opt
        const localization = client.localization.get(config.language)

        if (!isAdmin(message, config)) return message.reply(localization.general.no_perm).catch(console.error);
        let role = message.mentions.roles.first() || message.guild.roles.cache.get(args[0]);
        if (role) {
            return randomColor()
        } else {
            if (message.guild.id !== "301054974970822656") return;
            role = message.guild.roles.cache.get("353088160030523392");
            if (role) return randomColor()
        }

        function randomColor() {
            role.setColor(getRandomColor()).catch(console.error);
            setTimeout(randomColor, 5000);
        }

    },
    aliases: ['lightString', 'ls'],
    description: {french: "Change la couleur d'un role aléatoirement toute les 5 secondes.", english: 'Change a role color randomly every 5 seconds.'},
    help: [{cmd: "guirlande {role}", desc: "guirlande_desc_O"}],
    info: {dm: false, name: 'guirlande', category: "fun"}
}
