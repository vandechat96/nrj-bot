const {isAdmin} = require("../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {
        let msg = args.join(" ");
        const {config} = opt
        if (msg) {
            if (message.channel.type !== 'dm') message.delete().catch(console.error)
            if (!isAdmin(message, config) && (msg.includes('here') || msg.includes('everyone'))) return
                message.channel.send(msg).catch(console.error)
        }
    },
    aliases: ['say'],
    description: {french: "Envoie un message via le bot.", english: 'Send a message trough the bot.'},
    help: [{cmd: "echo {text}", desc: "echo_desc_0"}],
    info: {dm: true, name: 'echo', category: "fun"}
}
