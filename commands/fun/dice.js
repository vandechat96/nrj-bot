module.exports = {
    run: async (client, message, args, opt) => {
        let {config} = opt
        const localization = client.localization.get(config.language)
        args = message.content.slice(config.prefix.length + 5).split(';');

        let resultat = '';
        let total = 0
        let subtotal = 0;

        for (let i = 0; i < args.length; i++) {

            if (args[i]) {

                resultat += '\n# ';

                let info = args[i].split('d');
                let nbrDe = info[0];
                let valDe = info[1];

                if (isNaN(nbrDe) || isNaN(valDe) || !valDe || !nbrDe) return message.channel.send(localization.general.nan).catch(console.error)

                for (let j = 0; j < nbrDe; j++) {
                    let nbr = Math.floor((Math.random() * valDe) + 1)
                    subtotal += nbr
                    resultat += nbr + '  '
                }
                resultat += '= ' + subtotal
                total += subtotal
                subtotal = 0
            } else {
                return message.channel.send(localization.general.not_complete).catch(console.error)
            }

        }
        resultat += '\nTotal : ' + total

        message.channel.send(`\`\`\`md\n${localization.dice.result} : ${resultat}\`\`\``).catch(console.error)
    },
    aliases: ['de'],
    description: {french: "Lance les dés.", english: 'Roll the dice.'},
    help: [{cmd: "dice {number}d{value};...", desc: "dice_desc_0"}],
    info: {dm: false, name: 'dice', category: "fun"}
}
