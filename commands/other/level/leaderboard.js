module.exports = {
    run: async (client, message, args, opt) => {
        let lvl = require(client.commands.get('level'))
        args.splice(0, 0, 'leaderboard')
        lvl.run(client, message, args, opt)
    },
    aliases: [],
    description: {french: "Affiche le tableau des niveaux.", english: 'Display the leaderboard.'},
    help: [],
    info: {dm: false, name: 'leaderboard', category: "other"}
}
