module.exports = {
    run: async (client, message, args, opt) => {
        let lvl = require(client.commands.get('level'))
        args.splice(0, 0, 'top')
        lvl.run(client, message, args, opt)
    },
    aliases: [],
    description: {french: "Affiche le tableau des niveaux du serveur.", english: 'Display the server leaderboard.'},
    help: [],
    info: {dm: false, name: 'top', category: "other"}
}
