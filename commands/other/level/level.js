const Scores = require('../../../database/schemas/Scores')
const {menu} = require("../../../utils/utils");
module.exports = {
    run: async (client, message, args, opt) => {

        const {config} = opt
        const configj = require("../../../json/config.json");
        const localization = client.localization.get(config.language)

        let score = client.scores.get(message.author.id+''+message.guild.id)

        if (!score) {
            score = await Scores.findOne({'userId': message.author.id, 'guildId': message.guild.id})
            if (!score) {
                score = await Scores.create({
                    'userId': message.author.id,
                    'guildId': message.guild.id
                })
            }
        }

        score.points++;

        const curLevel = Math.floor(0.1 * Math.sqrt(score.points));

        if (curLevel > score.level) {
            //message.reply(`Félicitations ! Vous etes maintenant niveau  **${curLevel}** !`);
            score.level = curLevel;
        }

        client.scores.set(message.author.id+''+message.guild.id,  score)
        client.scoresChange.set(message.author.id+''+message.guild.id,score)

        const startsPrefix = message.content.startsWith(config.prefix)
        if (!startsPrefix && !message.mentions.users.has(client.user.id)) return

        //show lvl
        if (message.content.startsWith(config.prefix + "level")) {
            const user = message.mentions.users.first() || client.users.cache.get(args[0]);
            if (args[0]) {
                if (user) {
                    score = await Scores.findOne({'userId': user.id, 'guildId': message.guild.id})
                    if (!score) {
                        score = await Scores.create({
                            'userId': user.id,
                            'guildId': message.guild.id
                        })
                        score.save()
                    }
                    let txt = localization.level.other_level.replace('{name}', user).replace('{level}', score.level).replace('{points}', score.points)
                    message.channel.send(txt).catch(console.error);
                } else {
                    message.channel.send(localization.general.wrong_mention)
                }
            } else {
                let txt = localization.level.self_level.replace('{level}', score.level).replace('{points}', score.points)
                message.reply(txt).catch(console.error);
            }
        } else
            //set lvl
        if (args[0] === "setlvl") {
            args.splice(0, 1)
            if (message.author.id !== configj.ownerId) return
            const user = message.mentions.users.first() || client.users.cache.get(args[0]);
            if (!args[1]) {
                message.channel.send(localization.general.not_complete).catch(console.error)
            } else if (user) {

                score = await Scores.findOne({'userId': user.id, 'guildId': message.guild.id})

                if (!score) {
                    score = await Scores.create({
                        'userId': message.author.id,
                        'guildId': message.guild.id
                    })
                }
                score.points = Math.floor(Math.pow((args[1] / 0.1), 2));
                score.level = args[1];
                score.save()

                let txt = localization.level.other_level.replace('{name}', user).replace('{level}', score.level).replace('{points}', score.points)
                message.channel.send(txt).catch(console.error);
            } else {
                message.channel.send(localization.general.wrong_mention)
            }

        } else
            //leaderboard
        if (args[0] === 'leaderboard') {
            const list = await Scores.aggregate([{
                $group: {
                    _id: '$userId',
                    points: {$sum: '$points'}
                }
            }, {$sort: {points: -1}}])

            let title = 'Leaderboard'
            let format = (data, tour) => `${tour} ¤ <@${data._id}>, **${data.points}** points (level ${Math.floor(0.1 * Math.sqrt(data.points))})\n`;
            menu(client, message, list, format, title, false).catch(console.error)

        } else if (args[0] === 'top') {
            const list = await Scores.aggregate([{$sort: {points: -1}}, {$match: {guildId: message.guild.id}}])

            let title = 'Server top'
            let format = (data, tour) => `${tour} ¤ <@${data.userId}>, **${data.points}** points (level ${data.level})\n`;
            menu(client, message, list, format, title, false).catch(console.error)
        }

    },
    aliases: [],
    description: {
        french: "Montre son niveau ou celui de l'utilisateur choisis.",
        english: 'Show your level or the level of the chosen user.'
    },
    help: [],
    info: {dm: false, name: 'level', category: "other"}
}
