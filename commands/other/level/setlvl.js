module.exports = {
    run: async (client, message, args, opt) => {
        let lvl = require(client.commands.get('level'))
        args.splice(0, 0, 'setlvl')
        lvl.run(client, message, args, opt)
    },
    aliases: [],
    description: {french: "s", english: 's'},
    help: [],
    info: {dm: false, name: 'setlvl', category: "hidden"}
}
