const config = require("./json/"+process.argv[2]);
const discord = require('discord.js');
const {Intents} = require("discord.js");
const myIntents = new Intents();
myIntents.add(Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS,Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,Intents.FLAGS.DIRECT_MESSAGES,Intents.FLAGS.GUILD_MEMBERS);
const client = new discord.Client({intents: myIntents});
const {registerCommands, registerEvents, registerLocalizations} = require('./utils/registry');
const mongoose = require('mongoose');




(async () => {

await mongoose.connect(config["mongoUri"], {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
}).then(r => {
    const conn = r.connections[0]
    console.log(`Successfully connected to ${conn.name} on port ${conn.port}`)
});

    await client.login(config.token);
    client.ready = false

    client.commands = new Map();
    client.localization = new Map()
    client.oldListener = new Map()
    
    client.userWarns = new Map()

    client.servConfig = new Map()
    client.roleMenus = new Map()
    client.activeServers = new Map()
    client.hallOfFamePlayerResulsts = new Map()
    client.scores = new Map()
    client.scoresChange = new Map()

    client.color = config.color

    client.active = new Map()
    client.roleMenuCollectors = new Map()

    await registerLocalizations(client, '../strings')
    await registerEvents(client, '../events');
    await registerCommands(client, '../commands');

    setTimeout(()=>{
        console.log(client.readyAt)
        if (!client.ready) client.emit('ready')
    },15000)
})();
