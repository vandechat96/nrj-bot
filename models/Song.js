exports.Song = class  {
    get duration() {
        return this._duration;
    }

    set duration(value) {
        this._duration = value;
    }
    get userID() {
        return this._userID;
    }

    set userID(value) {
        this._userID = value;
    }
    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
    get chanID() {
        return this._chanID;
    }

    set chanID(value) {
        this._chanID = value;
    }
    get type() {
        return this._type;
    }

    set type(value) {
        this._type = value;
    }
    get url() {
        return this._url;
    }

    set url(value) {
        this._url = value;
    }
    get txt() {
        return this._txt;
    }

    set txt(value) {
        this._txt = value;
    }
    constructor(txt, url, type, chanID, name, userID, duration) {
        this._txt = txt
        this._url = url
        this._type = type
        this._chanID = chanID
        this._name = name
        this._userID = userID
        this._duration = duration
    }
}

