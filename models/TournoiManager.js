const Discord = require('discord.js');
const {getFrDate, firstUpper, getEnDate, sendDm} = require('../utils/utils')
const TournoiSave = require('../database/schemas/TournoiSave')
const numberEmoji = ['1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣', '🇦', '🇧', '🇨', '🇩', '🇪', '🇫', '🇬', '🇭', '🇮', '🇯'];
exports.TournoiManager = class {

    constructor(client, type, args, {guild,channel,author}) {
        this.client = client
        this.type = type
        this.args = args
        this.guild = guild
        this.channel = channel
        this.author = author

        this.language = client.servConfig.get(guild.id).language
        this.localization = client.localization.get(this.language)
        this.TMLocal = this.localization.tournoiManager
        this.typeTrad = this.TMLocal[this.type]
        this.switchNotif = false
        this.tier = args[0] || 8;
        this.heure = args[1] || '20h';
        if (isNaN(args[2])) this.jour = 0
        else this.jour = parseInt(args[2])

        if (!args[3]) this.nom = this.TMLocal.noTeamName;
        else {
            this.nom = "";
            for (let i = 3; i < args.length && !args[i].startsWith('['); i++) {
                this.nom += args[i] + " "
            }
        }
        try {
            this.setDate(this.heure.split('h'))
        } catch (e){
            throw e
        }


        if (type === 'training') {
            let trainingInfo = undefined
            try {
                trainingInfo = args.join(' ').split('[')
                if (trainingInfo[1]) trainingInfo = trainingInfo[1].split(']')
            } catch (e) {
                console.error(e)
            }
            this.code = trainingInfo[1] || this.TMLocal.noCode
            this.nomSalle = trainingInfo[0] || this.TMLocal.noRoomName
        }


        this.msgInvitServ = this.baseEmbed().setTitle(this.TMLocal.msgInvitServTitle).setDescription(this.TMLocal.msgInvitServDesc)

    }

    start(){
        let embed = this.baseEmbed()
            .setTitle(this.TMLocal.startEmbedTitle)
            .setDescription( this.TMLocal.startEmbedDesc )

        this.channel.send({embeds : [embed]}).then(async (msg)=>{
            await msg.react('1️⃣');
            await msg.react('2️⃣');
            await msg.react('3️⃣');
            await msg.react('4️⃣');
            await msg.react('5️⃣');
            await msg.react('6️⃣');
            await msg.react('✅');

            const filter = (r, user) => user.id === this.author.id;
            const collector = msg.createReactionCollector({filter, time: 5*60*1000,dispose:true});

            let list = new Array(6).fill(false)//[]
            let inviteTab =  [this.TMLocal.morning,this.TMLocal.noon,this.TMLocal.afternoon, this.TMLocal.evening,this.TMLocal.beforeTournament,this.TMLocal.duringDay]
            let react = (r) => {
                let index = numberEmoji.indexOf(r.emoji.name)
                if (index !== -1) list[index] = !list[index]
                else if (list.reduce((previousValue, currentValue) => previousValue||currentValue)) {
                    this.inviteTab = []
                    for (let i = 0; i < inviteTab.length; i++) {
                        if(list[i]) this.inviteTab.push(inviteTab[i])
                    }
                    msg.delete().catch(console.error)
                    this.startSend()
                }
            }
            collector.on('collect', react)
            collector.on('remove', react)
        })

    }

    async startSend() {
        this.playerList = [{
            userId: this.author.id,
            name: `\`${this.guild.members.cache.get(this.author.id).displayName}\``,
            type: 'OK',
            notified: true,
            notif: ['15min']
        }]
        this.launchRemind()

        let embed = this.firstBaseEmbed()
            .setTitle(`__${this.typeTrad.toUpperCase()}${(this.date.getDate() === new Date(this.now).getDate()) && this.language === "french" ? ' CE SOIR' : ''} !__`)
            .setTimestamp(this.date)
        let msgEmbed

        this.channel.send({embeds : [embed]}).then(async (msg) => {
            this.channel.send({embeds:[this.mainEmbed()]}).then(async (msg2) => {
                msgEmbed = msg2
                await msg2.react(':nrj_logo:505475221570977792').catch(console.error);
                await msg2.react('🆘');
            }).catch(console.error)
            await msg.react(':nrj_logo:505475221570977792');
            await msg.react('✅');
            await msg.react('❔');
            await msg.react('❌');
            await msg.react('🔔');
            await msg.react('🔕');
            await msg.react('🆘');
            await msg.react('📨');
            await msg.react('💬');

            this.msgId = msg.id
            this.saveTournoi(msg.id,msgEmbed.id).catch(console.error)
            this.launchCollect(msg,msgEmbed)

        }).catch(console.error)
    }

    launchCollect (msg,msgEmbed){
        const filter = (r, user) => !user.bot;
        let collector = msgEmbed.createReactionCollector({filter, time: this.diffMs});
        collector.on('collect', (r, user) => {
            if (r.emoji.id === "505475221570977792") return sendDm(user, {embeds:[this.msgInvitServ]})
            else if (r.emoji.name === "🆘") return sendDm(user, {embeds:[this.sosEmbed()]});
        })


        this.msgEmbed = msgEmbed
        collector = msg.createReactionCollector({filter, time: this.diffMs,dispose:true});

        collector.on('end',()=>{
            TournoiSave.findOneAndDelete({msgId:this.msgId}).exec()
            if (msg) msg.delete({timeout:30*60000}).catch(()=>console.error('msg_deleted err0'))
            if (msgEmbed) msgEmbed.delete({timeout:30*60000}).catch(()=>console.error('msg_deleted err1'))
        })
        let react = async (r, user) => {

            let member = this.guild.members.cache.get(user.id);
            if(!member) {
                await this.guild.members.fetch(this.author.id)
                member = this.guild.members.cache.get(user.id);
            }

            if (r.emoji.id === "505475221570977792") return sendDm(user, {embeds:[this.msgInvitServ]});

            let player = this.getPlayer(member)

            switch (r.emoji.name) {
                case "✅":
                    if (this.type === 'entrainement') {
                        let aList = this.playerList.filter((obj) => obj.type === 'A')
                        let bList = this.playerList.filter((obj) => obj.type === 'B')
                        if (aList.length + bList.length >= 36) return this.channel.send(this.TMLocal.noMoreRoom).then((msg) => msg.delete({timeout:5000}).catch(console.error))
                        player.type = aList < bList ? 'A' : 'B'
                        player.notified = true

                    } else if (this.playerList.filter((obj) => obj.type === 'OK').length < 27) {
                        player.type = 'OK'
                        player.notified = true
                    }
                    else this.channel.send(this.TMLocal.noMoreRoom).then((msg) => msg.delete({timeout:5000}).catch(console.error));
                    if(player.type === 'OK' && this.switchNotif) {
                        let embed = this.baseEmbed().setDescription(`${player.name} ${this.TMLocal.canNowParticipate} `)
                        sendDm(this.author,{embeds:[embed]}).catch(console.error)
                    }
                    break
                case "❔":
                    const embed = this.baseEmbed().setDescription(`${player.name}  ${this.TMLocal.canNoMoreParticipate}`)
                    if(player.type === 'OK' && this.switchNotif) sendDm(this.author,{embeds:[embed]}).catch(console.error)
                    player.type = 'OKO'
                    player.notified = false
                    break
                case "❌":
                    const e = this.baseEmbed().setDescription(`${player.name} ${this.TMLocal.canNoMoreParticipate} `)
                    if(player.type === 'OK' && this.switchNotif) sendDm(this.author,{embeds:[e]}).catch(console.error)
                    player.type = 'KO'
                    player.notified = false
                    break
                case "🔔":
                    this.changeNotif(player).catch(console.error)
                    break
                case "🔕":
                    player.notified = !player.notified
                    break
                case "🆘":
                    sendDm(user, {embeds:[this.sosEmbed()]}).catch(console.error)
                    break
                case "💬":
                    if (user.id !== this.author.id) return
                   this.switchNotif = !this.switchNotif
                    break
                case "📨":
                    if (user.id !== this.author.id) return
                    let ok = this.playerList.filter((obj) => ['OK', 'A', 'B'].indexOf(obj.type) !== -1)
                    for (let player of ok) {
                        let user = await this.client.users.fetch(player.userId)
                        let embed = this.baseEmbed()
                            .setTitle(this.TMLocal.invitMPTitle)
                            .setDescription(this.TMLocal.invitMPDesc
                                .replace('{name}',this.name)
                                .replace('{heure}',this.heure)
                                .replace('{tier}',this.tier)
                                .replace('{guild.name}',this.guild.name)
                                .replace('{tournoi_fixe}',this.type==='tournoi fixe'?this.TMLocal.msgMPTF:'')
                                )
                        sendDm(user, {embeds : [embed]}).catch(console.error)
                    }
                    break

            }

            msgEmbed.edit({embeds:[this.mainEmbed()]}).catch(console.error);
        }
        collector.on('collect', react);
        collector.on('remove', async (r,user)=>{

            let member = await this.guild.members.fetch(user.id);
            let player = this.getPlayer(member)
            switch (r.emoji.name) {
                case "🔕":
                    player.notified = !player.notified
                    break
                case "💬":
                    if (user.id !== this.author.id) return
                    this.switchNotif = !this.switchNotif
                    break
            }
            msgEmbed.edit(this.mainEmbed()).catch(console.error);
        })

    }
    getPlayer(member){
        let index = this.playerList.findIndex((obj) => obj.userId === member.id)

        let player
        if (index === -1) {
            player = {
                userId: member.id,
                name: `\`${member.displayName}\``,
                type: 'KO',
                notified: true,
                notif: ['15min']
            }
            this.playerList.push(player)
        } else {
            player = this.playerList[index]
        }
        return player
    }
    baseEmbed() {
        return new Discord.MessageEmbed()
            .setAuthor({name:this.client.user.username, iconURL:this.client.user.avatarURL()})
            .setColor(this.client.color)
    }

    firstBaseEmbed() {
        return this.baseEmbed()
            .setImage("https://cdn.discordapp.com/attachments/727923120354230416/727923293797089390/D1Dsn1RXcAI_KhM.jpg")
    }

    mainEmbed() {
        let okName = '__**'+this.TMLocal.firstTeam+' : **__\n\n';
        let aName = '';
        let bName = '';
        let okoName = '';
        let koName = '';

        if (this.type === 'entrainement') {
            let aList = this.playerList.filter((obj) => obj.type === 'A')
            let bList = this.playerList.filter((obj) => obj.type === 'B')

            for (let i = 0; i < (aList.length <= 9 ? 9 : aList.length); i++) {
                aName += numberEmoji[i] + ' ' + (aList[i] ? aList[i].name : '') + '\n'
            }
            for (let i = 0; i < (bList.length <= 9 ? 9 : bList.length); i++) {
                bName += numberEmoji[i] + ' ' + (bList[i] ? bList[i].name : '') + '\n'
            }

        } else {
            let ok = this.playerList.filter((obj) => obj.type === 'OK')
            for (let i = 0; i < (ok.length <= 9 ? 9 : ok.length); i++) {
                okName += `${numberEmoji[i%9]} ${ok[i]?(`${ok[i].name} ${!ok[i].notified?'🔕':''} ${(ok[i].userId === this.author.id) && this.switchNotif?'💬':''}`):''} \n`
                if (i === 8 && this.type === "tournoi") okName += this.TMLocal.secondTeam
                if (i === 17 && this.type === "tournoi") okName += this.TMLocal.secondTeam
            }
        }

        let oko = this.playerList.filter((obj) => obj.type === 'OKO')
        let ko = this.playerList.filter((obj) => obj.type === 'KO')

        for (let i = 0; i < oko.length; i++) {
            okoName += numberEmoji[i] + ' ' + (oko[i] ? oko[i].name : '') + '\n'
        }
        for (const player of ko) {
            koName += player.name + '\n'
        }

        let description = "\n"
        if (this.type === "tournoi" || this.type === "tournoi fixe") description += this.TMLocal.teamName+` : ${this.nom}\n`
        description +=
            this.TMLocal.teamLeader+` : ${this.author.username}\n` +
            this.TMLocal.date+` : ${this.dateFR}\n` +
            this.TMLocal.start+` ${this.language==='french'?(this.typeTrad[0] === 'e'? "de l'" : "du "):''}${this.typeTrad} :  ${this.heure}\n` +
            this.TMLocal.invitHour+` :  ${this.inviteTab.reduce((prevVal, currVal) => prevVal + ',  '+currVal)}\n` +
            this.TMLocal.tier+` : ${this.tier}`
        if (this.type === "training" && this.nom) description += this.TMLocal.advClan+` : ${this.nom}`
        description += this.type === "training" ? this.TMLocal.roomName+` : ${this.nomSalle}` : ''//`\nHeure d'invitaion : ${this.nomSalle}`
        if (this.code && this.code !== this.TMLocal.noCode) description += `\nCode : ${this.code}`
        description += "\n\u200b"

        let embed = new Discord.MessageEmbed()
            .setColor(16564228)
            .setTitle(this.TMLocal.detailTitle.replace('{type}',this.typeTrad))
            .setDescription(description)

        if (this.type === 'tournoi fixe') {
            embed.addField(this.TMLocal.THFTitle, this.TMLocal.THFDesc)
        }

        if (this.type === 'entrainement') {

            embed.addField(
                this.TMLocal.noteTitle,
                this.TMLocal.noteDesc+
                "\n\u200b")
                .addField(this.TMLocal.teamA
                    ,
                    aName +
                    "\u200b", true)
                .addField(
                    this.TMLocal.teamB,
                    bName +
                    "\u200b", true)
        } else {
            embed.addField(this.TMLocal.teamP
                ,
                okName +
                "\u200b")
        }


        embed.addField(this.TMLocal.teamPS
            ,
            "\u200b\n" + okoName + "\u200b\n")
            .addField(this.TMLocal.teamI
                ,
                koName +
                "\n\u200b" +
                this.TMLocal.msgHelp+
                "\n");
        return embed;

    }


    setDate(argsDate) {
        this.date = new Date();
        this.date.setDate(this.date.getDate() + this.jour);
        if (!isNaN(argsDate[0]) && argsDate[0] >= 0 && argsDate[0] <= 23) this.date.setHours(argsDate[0]);
        this.date.setMinutes(0);
        if (argsDate[1] && !isNaN(argsDate[1]) && argsDate[1] <= 59 && argsDate[0] >= 0) this.date.setMinutes(argsDate[1]);
        this.date.setSeconds(0);
        this.now = Date.now();
        if (this.date < this.now) throw this.channel.send("Mauvaise heure").catch(console.error);
        this.diffMs = Math.abs(this.date - this.now);
        this.dateFR = this.language==='french'?getFrDate(this.date):getEnDate(this.date);
        //this.dateEN = getEnDate(this.date);
    }

    launchRemind() {
        let now = Date.now();
        let ms = Math.abs(this.date - now);
        let ms15
        let ms30 = 0
        let ms1 = 0
        if (ms > 15 * 60 * 1000) {
            ms15 = ms - 15 * 60 * 1000;
            ms30 = ms - 30 * 60 * 1000;
            ms1 = ms - 60 * 60 * 1000;

        } else ms15 = ms;

        setTimeout(async () => {
            let notified = this.playerList.filter((obj) => obj.notified).filter((obj) => obj.notif.indexOf('15min')!==-1)
            for (let player of notified) {
                let user = await this.client.users.fetch(player.userId)
                let embed =this.baseEmbed().setDescription(this.TMLocal.msg15min
                    .replace('{type}',firstUpper(this.typeTrad))
                    .replace('{tier}',this.tier)
                    .replace('{guild.name}',this.guild.name)
                )
                sendDm(user, {embeds:[embed]}).catch(console.error)
            }
        }, ms15);

        setTimeout(async () => {
            let notified = this.playerList.filter((obj) => obj.notified).filter((obj) => obj.notif.indexOf('30min')!==-1)
            for (let player of notified) {
                let user = await this.client.users.fetch(player.userId)
                let embed = this.baseEmbed().setDescription(
                    this.TMLocal.msg30min
                        .replace('{type}',firstUpper(this.typeTrad))
                        .replace('{tier}',this.tier)
                        .replace('{guild.name}',this.guild.name)
                )
                sendDm(user, {embeds:[embed]}).catch(console.error)
            }
        }, ms30);

        setTimeout(async () => {
            let notified = this.playerList.filter((obj) => obj.notified).filter((obj) => obj.notif.indexOf('1h')!==-1)
            for (let player of notified) {
                let user = await this.client.users.fetch(player.userId)
                let embed = this.baseEmbed().setDescription(
                    this.TMLocal.msg1h
                        .replace('{type}',firstUpper(this.typeTrad))
                        .replace('{tier}',this.tier)
                        .replace('{guild.name}',this.guild.name)
                )
                sendDm(user, {embeds:[embed]}).catch(console.error)
            }
        }, ms1);
    }

    async changeNotif(player) {
        let user = await this.client.users.fetch(player.userId)
        player.notified = true
        player.notif =[]
        const embed = this.baseEmbed()
            .setTitle(this.TMLocal.notifTitle)
            .setDescription(this.TMLocal.notifDesc)

        let msg = await sendDm(user, {embeds : [embed]})
        await msg.react('🕒')
        await msg.react('🕕')
        await msg.react('🕛')
        await msg.react('🔕')
        await msg.react('✅')

        const filter = (r, user) => !user.bot;
        const collector = msg.createReactionCollector({filter, time: 60000, dispose: true});
        let m15 = false
        let m30 = false
        let h1 = false

        let react = (r, user) => {
            switch (r.emoji.name) {
                case '🕒':
                    m15 = !m15
                    break
                case '🕕':
                    m30 = !m30
                    break
                case '🕛':
                    h1 = !h1
                    break
                case '🔕':
                    player.notified = false
                    break
                case '✅':
                    collector.stop()
                    if (m15) player.notif.push('15min')
                    if (m30) player.notif.push('30min')
                    if (h1) player.notif.push('1h')
                    sendDm(user, this.baseEmbed().setTitle(this.TMLocal.changesSave))
                    this.msgEmbed.edit(this.mainEmbed()).catch(console.error);
                    break
            }
        }
        collector.on('collect', react)
        //collector.on('remove', react)
    }

    sosEmbed(){
        return this.baseEmbed()
            .setTitle(this.TMLocal.sosTitle)
            .addField(this.TMLocal.sosDescTitle,this.TMLocal.sosDesc.replaceAll('{type}',this.typeTrad))

    }

   async saveTournoi(msgId,msgEmbedId) {
        this.save = await TournoiSave.create({
            playerList:this.playerList,
            inviteTab:this.inviteTab,
            args:this.args,
            msgId,
            msgEmbedId,
            type:this.type,
            guildId:this.guild.id,
            channelId:this.channel.id,
            authorId:this.author.id,
        });
        this.saveLoop(this.playerList,this.msgId)
    }
    saveLoop(playerList,msgId){
        TournoiSave.findOneAndUpdate({msgId},{playerList:playerList}).exec()
        setTimeout(()=>this.saveLoop(playerList,msgId),60000)
    }

    restart(tournoi,msgEmbed,msg){
        this.playerList = tournoi.playerList
        this.inviteTab = tournoi.inviteTab
        this.launchCollect(msg,msgEmbed)
        this.saveLoop(this.playerList,tournoi.msgId)
        this.launchRemind()
    }
}