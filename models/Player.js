const ytdl = require('ytdl-core-discord');
const {setMetadata,baseEmbed} = require('../commands/music/nowplaying');
exports.Player = class {
    constructor(client,data,voiceChannel) {
        this.client = client
        this.data = data;
        this.voiceChannel = voiceChannel;
        this.initialized = false;
    }

    async init(){
        if(this.initialized) return

        if(!this.voiceChannel) return
        this.guild = this.voiceChannel.guild
        if (!this.data.connection) this.data.connection = await this.voiceChannel.join()
        if (!this.guild.me.voice.deaf) this.guild.me.voice.setDeaf(true).catch(console.error)
        if (!this.data.volume) this.data.volume = 20;
        if (!this.data.queueIndex) this.data.queueIndex = 0;
        if (!this.data.queue) this.data.queue = []

        this.client.active.set(this.voiceChannel.guild.id, this.data);
        this.initialized = true
    }

    async add (song){
        if (!this.data.queue) this.data.queue = []
        let index = this.data.queue.findIndex((sg) => sg.url === song.url)
        if (index === -1) this.data.queue.push(song)

        if (this.data.queue.length === 1) await this.play(song)
        else await this.npMsg(song, true)

        this.client.active.set(this.voiceChannel.guild.id, this.data);
    }

    async play (song,seek = 0){
        if(!this.initialized) await this.init()

        let type = "unknown"
        let stream = song.url
        if (song.type === 'yt') {
            stream = await ytdl(song.url).catch(console.error)
            type="opus"
        }

        try {
            this.data.dispatcher = await this.data.connection.play(stream, {seek,highWaterMark: 1 << 25,type})
        } catch (e){
            return console.error(e)
        }



        if (!this.data.dispatcher) throw "error : cannot play song"

        await this.npMsg(song)

        this.data.nowPlaying = song
        this.data.dispatcher.setVolume(this.data.volume / 100)
        this.data.dispatcher.on('finish', ()=>this.finish(this))
        this.data.dispatcher.on('error',console.error)
        this.client.active.set(this.voiceChannel.guild.id, this.data);

    }

     finish(player) {
        if (isNaN(player.data.queueIndex)) return
        if (player.data.loop === 1) player.data.queueIndex--
        else if (player.data.loop === 0 && player.data.queueIndex >= player.data.queue.length - 1) player.data.queueIndex = -1

        if (player.data.queueIndex < player.data.queue.length - 1) {
            let song = player.data.queue[++player.data.queueIndex]
            player.play(song).catch(console.error)
        } else {
            player.data.dispatcher = undefined
            player.data.nowPlaying = undefined
            player.data.queueIndex = 0
        }
         player.client.active.set(player.voiceChannel.guild.id, player.data);
    }

    async npMsg(song,queue=false){
        await setMetadata(song)
        const config = this.client.servConfig.get(this.guild.id)
        const localization = this.client.localization.get(config.language)
        const embed = baseEmbed(this.client,localization,song).setTitle("__**"+(queue?localization.nowplaying.queue:localization.nowplaying.np)+"**__")
        this.guild.channels.cache.get(song.chanID).send({embeds : [embed]})
    }
}