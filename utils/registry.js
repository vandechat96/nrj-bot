const fs = require('fs').promises;
const path = require('path');
const {checkCommandModule, checkProperties} = require('./validate');


async function registerCommands(client, dir) {
    let files = await fs.readdir(path.join(__dirname, dir));
    // Loop through each file.
    for (let file of files) {
        let stat = await fs.lstat(path.join(__dirname, dir, file));
        if (stat.isDirectory()) // If file is a directory, recursive call recurDir
            registerCommands(client, path.join(dir, file));
        else {
            // Check if file is a .js file.
            if (file.endsWith(".js")) {
                let cmdName = file.substring(0, file.indexOf(".js"));
                try {
                    let cmdModule = path.join(__dirname, dir, file);
                    if (checkCommandModule(cmdName, cmdModule)) {
                        if (checkProperties(cmdName, cmdModule)) {
                            let cmd = require(cmdModule);
                            if (client.user.id === '719987677122003036' && (cmd.info.category === 'blitz' || cmd.info.category === 'nrj')) continue
                            let aliases = cmd.aliases
                            delete require.cache[require.resolve(cmdModule)];
                            client.commands.set(cmdName, cmdModule);
                            if (aliases.length !== 0)
                                aliases.forEach(alias => client.commands.set(alias, cmdModule));
                        }
                    }
                } catch (err) {
                    console.log(err);

                }
            }
        }
    }
}

async function registerEvents(client, dir) {
    let files = await fs.readdir(path.join(__dirname, dir));
    // Loop through each file.
    for (let file of files) {
        let stat = await fs.lstat(path.join(__dirname, dir, file));
        if (stat.isDirectory()) // If file is a directory, recursive call recurDir
            registerEvents(client, path.join(dir, file));
        else {
            // Check if file is a .js file.
            if (file.endsWith(".js")) {
                let eventName = file.substring(0, file.indexOf(".js"));
                try {
                    delete require.cache[require.resolve((path.join(__dirname, dir, file)))]
                    let eventModule = require(path.join(__dirname, dir, file));
                    let listener =  eventModule.bind(null, client)

                    let oldListener = client.oldListener.get(eventName)
                    client.oldListener.set(eventName,listener)

                    if(oldListener)client.removeListener(eventName, oldListener)
                    client.on(eventName,listener);

                } catch (err) {
                    console.log(err);
                }
            }
        }
    }
}

async function registerLocalizations(client, dir) {
    let files = await fs.readdir(path.join(__dirname, dir));
    // Loop through each file.
    for (let file of files) {
        let stat = await fs.lstat(path.join(__dirname, dir, file));
        if (stat.isDirectory()) // If file is a directory, recursive call recurDir
            registerLocalizations(client, path.join(dir, file));
        else {
            // Check if file is a .json file.
            if (file.endsWith(".json")) {
                let localName = file.substring(0, file.indexOf(".json"));
                try {
                    delete require.cache[require.resolve(path.join(__dirname, dir, file))]
                    let localModule = require(path.join(__dirname, dir, file));
                    client.localization.set(localName, localModule);
                } catch (err) {
                    console.log(err);
                }
            }
        }
    }
}

async function registerClean(client, dir) {
    let files = await fs.readdir(path.join(__dirname, dir));
    // Loop through each file.
    for (let file of files) {
        let stat = await fs.lstat(path.join(__dirname, dir, file));
        if (stat.isDirectory()) // If file is a directory, recursive call recurDir
            registerClean(client, path.join(dir, file));
        else {
            try {
                delete require.cache[require.resolve(path.join(__dirname, dir, file))]
            } catch (e){
                console .log(e)
            }

        }
    }
}

module.exports = {
    registerEvents,
    registerCommands,
    registerLocalizations,
    registerClean
};