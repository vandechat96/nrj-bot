const {Request} = require("./request");
async function getList(station){

	let date = new Date()
	let ajd = date.getFullYear()+'-'+(date.getMonth()+1<10?'0'+(date.getMonth()+1):(date.getMonth()+1))+'-'+(date.getDate()<10?'0'+(date.getDate()):(date.getDate()))
	

	let url = 'history.radiorecord.ru/air/'+station+'/'+ajd+'/'
	const http = require('http')
	const options = {
		host: 'history.radiorecord.ru',
		port: 80,
		path: '/air//'+station+'/' + ajd + '/'
	};

	let body = []
	return new Promise((resolve, reject) => {
		http.get(options, function(res) {
		  //console.log("Got response: " + res.statusCode);
			if(res.statusCode !== 200) reject("Got error: " + res.statusCode);
		  res.on('data', function (chunk) {
		    add(''+chunk)
		  });
		  res.on('end',()=>{
		  		resolve(body);
		  })
		}).on('error', function(e) {
		  reject("Got error: " + e.message);
		});
	});

	function add(chunk){
		let args = chunk.split('<').join('€').split('>').join('€').split('€')
		for (let i = 0; i < args.length; i++) {
			if(args[i].startsWith('a')){
				let file = url+args[i].slice(8,args[i].length-1)
				let name = asciiToStr(args[i].slice(24,args[i].length-5))
				if (args[i+1]) name = (args[i+1].slice(11,args[i+1].length-4))
				if (name !== ' - ') body.push({file:file,name:name})
			}
		}
	}

}

function asciiToStr(str){
	let r = ''
	for (let i = 0; i < str.length; i++) {
		if (str[i] === '%'){
			let code = str.slice(i+1,i+3)
			r+=String.fromCharCode(parseInt(code, 16));
			i+=2
		} else{
			r+=str[i]
		}
		
	}
	return r.slice(1,r.length)
}

exports.getList = getList;

exports.getStations = function(){
	return new Promise( async (resolve) => {
		let radios = await Request('http://history.radiorecord.ru/air/')
		radios = radios.split('<').join('€').split('>').join('€').split('€')
		let list = []
		for (let i = 0; i < radios.length; i++) {
			if(radios[i].startsWith('a')){
				list.push(radios[i].slice(8,radios.indexOf('/')-1))
			}
		}
		resolve(list)
	})

}


