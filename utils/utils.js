const {Permissions} = require("discord.js");
exports.isEmpty = function (obj) {
    for (const key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
};

exports.validURL = function (str) {
    try {
        new URL(str);
    } catch (_) {
        return false;
    }

    return true;
};

exports.getFrDate = function (date) {
    let days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    let months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];

    let day = date.getDay();
    let month = date.getMonth();
    let year = date.getFullYear();

    return days[day] + " " + date.getUTCDate() + ' ' + months[month] + ' ' + year
}

exports.getEnDate = function (date) {
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let months = ['January', 'February', 'Mart', 'April', 'May', 'June', 'July', 'Augustus', 'September', 'October', 'November', 'December'];

    let day = date.getDay();
    let month = date.getMonth();
    let year = date.getFullYear();

    return days[day] + " " + date.getUTCDate() + ' ' + months[month] + ' ' + year
}

exports.isAdmin = function (message, config) {
    const configj = require("../json/config.json");
    let ok = false
    let roles = config.adminRoles
    for (let role of message.member.roles.cache) {
        if (roles.indexOf(role[0]) !== -1) {
            ok = true
            break
        }
    }
    return ok || message.member.permissions.has(Permissions.FLAGS.ADMINISTRATOR) || message.author.id === configj.ownerId;
}

exports.sendDm = async function (user, message) {
    if (!user.dmChannel) {
        let dm = await user.createDM().catch(console.error)
        if (dm) return await dm.send(message).catch(console.error)
    } else {
        return await user.send(message).catch(console.error)
    }
}

exports.firstUpper = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
}

exports.separateBrack = function (args, index) {
    let r = ""
    if (args[index]) {
        r = (args[index].startsWith("[") ? args[index].substring(1, args[index].length) : args[index]) + " ";
        for (let i = 3; i < args.length && !args[i].startsWith('['); i++) {
            r += args[i] + " "
        }
        r = r.substring(0, r.length - (r.endsWith("]") ? 4 : 2))
    }
    return r
}

exports.getRandomColor = function () {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

exports.menu = async function (client, message, list, format, title, inverted) {
    const discord = require('discord.js')
    let min = 0;
    let max = 10;
    let fwd = 10
    let bwd = -10;
    const absMax = Math.ceil(list.length / 10) * 10
    if (inverted) {
        max = absMax;
        min = max - 10;
        fwd = -10
        bwd = 10;
    }


    function Embed(min, max) {
        let embed = new discord.MessageEmbed()
            .setAuthor({name:client.user.username, iconURL:client.user.avatarURL()})
            .setColor(client.color)
            .setFooter({text:"© " + client.user.username, iconURL:client.user.avatarURL()})
            .setTimestamp();

        let tour = 0;
        let board = "";

        for (const item of list) {
            tour++;
            if (inverted) board = format(item, tour) + board
            else board += format(item, tour)
            if (tour === min) board = '';
            if (tour === max) break
        }
        embed.setTitle("__**" + title + " " + (inverted ? absMax - max + 10 : max) / 10 + "**__")
        if (board.length < 2048) {
            embed.setDescription(board);
        } else {
            console.error('lol plus de 2048')
        }

        return embed
    }

    message.channel.send({embeds: [Embed(min, max)]}).then(
        async function msg(msg) {
            await msg.react('%E2%AC%85');
            await msg.react('%E2%9E%A1');

            const filter = (r, user) => {
                return user.id === message.author.id;
            }
            const collector = msg.createReactionCollector( {filter, time: 90000, dispose: true})
            let react = async r => {

                if (r.emoji.name === '⬅') {
                    if (min <= 0) return;
                    min += bwd;
                    max += bwd;
                    msg.edit({embeds: [Embed(min, max)]}).catch(console.error)
                } else if (r.emoji.name === '➡') {
                    min += fwd;
                    max += fwd;
                    if (min < list.length) msg.edit({embeds: [Embed(min, max)]}).catch(console.error)
                    else {
                        min -= fwd;
                        max -= fwd;
                    }
                }
            }

            collector.on('collect', react)
            collector.on('remove', react)
        })
}

exports.mapSize = function (map) {
    let size = 0, key;
    for (key in map) {
        if (map.hasOwnProperty(key)) size++;
    }
    return size
}

exports.getMetadata = function (url) {

    let spawn = require('child_process').spawn;

    let cmd = 'ffprobe';
    let args = [
        '-v', 'quiet',
        '-print_format', 'json',
        '-show_format',
        url
    ];

    let metadata = {}
    let proc = spawn(cmd, args);
    return new Promise((resolve, reject) => {
        proc.stdout.on('data', function (data) {
            metadata += data.toString()
        });

        proc.on('close', function () {
            metadata = JSON.parse(metadata.slice(15, metadata.length))
            if (metadata.hasOwnProperty('format')) resolve(metadata.format)
            else reject('error while analyzing metadata')

        });
    });
}

exports.toHMS = function (nbr) {
    let h = Math.floor(nbr / 3600)
    nbr %= 3600
    let m = Math.floor(nbr / 60)
    let s = nbr % 60
    return `${h > 0 ? h + 'h' : ''} ${m > 0 ? m + 'm' : ''} ${s}s`
}


exports.roleMenu = async function (client, menu) {
    if (!menu.guildId) return console.log('gid');
    let guild = client.guilds.cache.get(menu.guildId);
    let channel = guild.channels.cache.get(menu.channelId);
    if (!channel) return console.log('chan');
    let msg = await channel.messages.fetch(menu.msgId)
    if (!msg) return console.log('menu desactivé')
    let {rolesMap, inv} = menu;
    const config = client.servConfig.get(guild.id)
    const localization = client.localization.get(config.language)

    const filter = (r, user) => !user.bot;
    const collector = msg.createReactionCollector(filter, {dispose: true});


    client.roleMenuCollectors.set(msg.id, collector)
    const removeRole = async (r, user) => {
        let member = msg.guild.members.cache.get(user.id);
        if(!member) {
            await this.guild.members.fetch(this.author.id)
            member = msg.guild.members.cache.get(user.id);
        }
        rolesMap.forEach((roleID, emoji) => {
            if (emoji === r.emoji.name) {
                let role = msg.guild.roles.cache.get(roleID);
                if (!role) return module.exports.sendDm(user, localization.rolemenu.nom_role);
                    member.roles.remove(role).then(() => {
                        module.exports.sendDm(user, localization.rolemenu.rm_role + role.name)
                    }).catch(console.error);
            }
        })
    }
    const addRole = async (r, user) => {
        let member = msg.guild.members.cache.get(user.id);
        if(!member) {
            await this.guild.members.fetch(this.author.id)
            member = msg.guild.members.cache.get(user.id);
        }
        rolesMap.forEach((roleID, emoji) => {
            if (emoji === r.emoji.name) {
                let role = msg.guild.roles.cache.get(roleID);
                if (!role) return module.exports.sendDm(user, localization.rolemenu.nom_role);
                if (!inv) member.roles.add(role).then(() => {
                    module.exports.sendDm(user, localization.rolemenu.add_role + role.name)
                }).catch(console.error)
            }
        })
    }

    collector.on('collect', addRole)
    collector.on('remove', removeRole)
};

exports.asyncForEach = async function (array, callback){
    for (let elem of array){
        await callback(elem);
    }
}