exports.run = async (client, message) => {

    let commChans = [];
    for (const guild of client.guilds.cache) {
        let config = client.servConfig.get(guild[0]);
        if (!config) continue;
        if (config.commChannel) commChans.push(config.commChannel)
    }

    let attachments = [];
    for (const attachment of message.attachments) {
        attachments.push(attachment[1].url)
    }


    let channel;
    for (const channelID of commChans) {
        channel = client.channels.cache.get(channelID);
        if (channel && channel.id !== message.channel.id) {
            try {
                const webhooks = await channel.fetchWebhooks();
                let webhook = webhooks.first();

                if (!webhook) {
                    webhook = await channel.createWebhook(client.user.username, {
                        avatar: client.user.avatarURL(),
                    }).catch(console.error);
                }

                await webhook.send(message.content, {
                    username: message.author.username,
                    avatarURL: message.author.avatarURL(),
                    files: attachments,
                });
            } catch (error) {
                console.error('Error trying to send: ', error);
            }


        }
    }

};
