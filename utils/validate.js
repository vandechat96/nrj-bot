module.exports.checkCommandModule = (cmdName, cmdModule) => {
    cmdModule = require(cmdModule)
    if(!cmdModule.hasOwnProperty('run'))
        throw new Error(`${cmdName} command module does not have property 'run'`);
    if(!cmdModule.hasOwnProperty('description'))
        throw new Error(`${cmdName} command module does not have property 'description`);
    if(!cmdModule.hasOwnProperty('aliases'))
        throw new Error(`${cmdName} command module does not have property 'aliases'`);
    if(!cmdModule.hasOwnProperty('help'))
        throw new Error(`${cmdName} command module does not have property 'help'`);
    if(!cmdModule.hasOwnProperty('info'))
        throw new Error(`${cmdName} command module does not have property 'info'`);
    return true;
}
module.exports.checkProperties = (cmdName, cmdModule) => {
    cmdModule = require(cmdModule)
    if(typeof cmdModule.run !== 'function')
        throw new Error(`${cmdName} command: run is not a function`);
    if(typeof cmdModule.description !== 'object')
        throw new Error(`${cmdName} command: description is not a object`);
    if(!Array.isArray(cmdModule.aliases))
        throw new Error(`${cmdName} command: aliases is not an Array`);
    if(typeof cmdModule.help !== 'object')
        throw new Error(`${cmdName} command: help is not an object`);
    if(typeof cmdModule.info !== 'object')
        throw new Error(`${cmdName} command: info is not a object`);
    return true;
}
