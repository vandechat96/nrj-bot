const Discord = require('discord.js');
const fs = require("fs");
const configj = require("../json/config.json");
const webhook = new Discord.WebhookClient(configj.wh1, configj.wh2);

exports.log = async function (content, actionLog) {
    if (actionLog) content += ` cmd used in guild  ${actionLog.guild.name} (${actionLog.guild.id}) by ${actionLog.member.user.username} (${actionLog.member.id})`;
    fs.appendFile("./nrjlog.txt", content + '\n', (err) => {
        if (err) console.error(err)
    });
    if (content.length < 2000) {
        webhook.send('`NRJBot` | `' + content + '`')
            .catch(err => console.log(`Failed to send. ${err} Console log:`, content));
    } else {
        webhook.send('txt file (due to oversized webhook)', {
            file: {
                attachment: Buffer.from(content, 'utf8'),
                name: 'WebHook.txt'
            }
        })
            .catch(err => console.log(`Failed to send. ${err} Console log:`, content));
    }
};

exports.serverLog = async function (client, guild, config, content) {
    try {
        if (!config) return;
        if (!config.logChannel) return;
        if (!client.channels.get(config.logChannel)) return;
        let channel = client.channels.get(config.logChannel);
        if (channel) channel.send(content).catch(console.error)
    } catch (error) {
        console.error(error)
    }

};