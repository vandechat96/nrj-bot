const banword = require('../json/banword.json')
const {isAdmin} = require("./utils");
exports.run = async (client, message,config) => {

    if (isAdmin(message,config)) return
    for (let word of banword){

        let triggered = false
        if (word.endsWith("*") && message.content.toLowerCase().includes(word.replace('*',''))) triggered = true
        else if (message.content.trim().split(/\s+/).findIndex((elem)=>elem.toLocaleLowerCase() === word) !== -1) triggered = true

        if (triggered){
            message.delete().catch(console.error)
            message.reply('Surveille ton langage ! Raison : ||'+word+"||")
            let id = message.author.id
            let warns = client.userWarns.get(id) || []
            warns.push('banword')
            client.userWarns.set(id,warns)
            
            if (warns.length >= 3){
                const role = message.guild.roles.cache.get(config.muteRole)
                let roles = []
                if (role) roles = [role]
                await message.member.roles.set(roles)
                setTimeout(async ()=>await message.member.roles.set([]),30*60*1000)
            }
            setTimeout(()=>client.userWarns.get(id).pop(),30*60*1000)
        return
        }
    }

};
