const mongoose = require('mongoose')

const ScoresSchema = new mongoose.Schema({
    guildId: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    userId: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    points: {
        type: mongoose.SchemaTypes.Number,
        required: true,
        default: 0
    },
    level: {
        type: mongoose.SchemaTypes.Number,
        required: true,
        default: 0,
    },
})

module.exports = mongoose.model('Scores', ScoresSchema)