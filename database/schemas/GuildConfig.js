const mongoose = require('mongoose')

const GuildConfigSchema = new mongoose.Schema({
    guildId: {
        type: mongoose.SchemaTypes.String,
        required: true,
        unique: true,
    },
    prefix: {
        type: mongoose.SchemaTypes.String,
        required: true,
        default: 'N!',
    },
    cooldown: {
        type: mongoose.SchemaTypes.Number,
        required: true,
        default: 5,
    },
    language: {
        type: mongoose.SchemaTypes.String,
        required: true,
        default: 'french',
    },
    logChannel: {
        type: mongoose.SchemaTypes.String,
        required: false,
    },
    welcomeChannel: {
        type: mongoose.SchemaTypes.String,
        required: false,
    },
    commChannel: {
        type: mongoose.SchemaTypes.String,
        required: false,
    },
    adminRoles: {
        type: mongoose.SchemaTypes.Array,
        required: false,
    },
    muteRole: {
        type: mongoose.SchemaTypes.String,
        required: false,
    },
    baseRole: {
        type: mongoose.SchemaTypes.String,
        required: false,
    }
})

module.exports = mongoose.model('GuildConfig', GuildConfigSchema)