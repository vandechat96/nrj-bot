const mongoose = require('mongoose')

const ActiveServersSchema = new mongoose.Schema({
    guildId: {
        type: mongoose.SchemaTypes.String,
        required: true,
        unique: true,
    },
    active: {
        type: mongoose.SchemaTypes.Boolean,
        required: true,
        default: true,
    },
})

module.exports = mongoose.model('ActiveServers', ActiveServersSchema)