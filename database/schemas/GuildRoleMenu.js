const mongoose = require('mongoose')

const GuildRoleMenuSchema = new mongoose.Schema({
    guildId: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    channelId: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    msgId: {
        type: mongoose.SchemaTypes.String,
        required: true,
        unique: true,
    },
    rolesMap: {
        type: mongoose.SchemaTypes.Map,
        required: true,
    },
    inv: {
        type: mongoose.SchemaTypes.Boolean,
        required: false,
        default: false,
    }
})

module.exports = mongoose.model('GuildRoleMenu', GuildRoleMenuSchema)