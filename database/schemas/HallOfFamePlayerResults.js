const mongoose = require('mongoose')

const HallOfFameSchema = new mongoose.Schema({
    guildId: {
        type: mongoose.SchemaTypes.String,
        required: true,
        unique: true,
    },
})

module.exports = mongoose.model('HallOfFamePlayerResults', HallOfFameSchema)