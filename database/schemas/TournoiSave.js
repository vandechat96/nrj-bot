const mongoose = require('mongoose')

const TournoiSaveSchema = new mongoose.Schema({
    playerList: {
        type: mongoose.SchemaTypes.Array,
        required: true,
    },
    inviteTab: {
        type: mongoose.SchemaTypes.Array,
        required: true,
    },
    args: {
        type: mongoose.SchemaTypes.Array,
        required: true,
    },
    type: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    guildId: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    channelId: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    msgId: {
        type: mongoose.SchemaTypes.String,
        required: true,
        unique:true,
    },
    msgEmbedId: {
        type: mongoose.SchemaTypes.String,
        required: true,
        unique:true,
    },
    authorId: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },

})

module.exports = mongoose.model('TournoiSave', TournoiSaveSchema)